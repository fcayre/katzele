.DELETE_ON_ERROR:
# Pour copier les fichiers d'un endroit à un autre
mk = install -D /dev/stdin

# On créé des variables contenant la liste des fichiers qui nous intéressent
# Dans sources les md à transformer en html
# Dans annexfiles les fichiers qui gravitent autour d'une page
# Dans allfiles l'union des deux pour générer le dossier raw
sources      != find src -type f -name '*.md'
annexfiles   != find src -type f -not -name '*.md'
allfiles      = $(sources) $(annexfiles)
# find src -type f

# On construit dynamiquement les règles à invoquer avec des substitutions de
# chaînes de caractères
# Ex: Pour pages on prend tous les chemins de fichiers récupérés dans sources
#     On substitue src/ par root/ et l'extension md par html
#     Le fichier source "src/truc/bidule.md" donnera donc
#     "root/truc/bidule.html"
# Même mécanique pour les raw et les fichiers annexes
pages         = ${sources:src/%.md=root/%.html}
annexrules    = ${annexfiles:src/%=root/%}
rawrules      = ${allfiles:src/%.md=root/%.md}

# On appelle toutes les règles pour produire tous les fichiers
# nécessaires
# Pour chacune make va tenter de trouver une règle correspondante dans la
# liste qui suit
all: ${pages} ${annexrules} ${rawrules} root/style.css root/events.atom
test: all; busybox httpd -h root -p 8000 -fvv

clean:; rm -r root/*

# Règle pour générer le css
root/style.css: rc/style.css; cp $< $@

# Règle pour générer l'html depuis les fichiers md
# Va matcher toutes les règles stockées dans la variable pages
# Chaque fichier html requiert sont équivalent md dans src et le script
# page
# Ce que % match dans la cible sera substitué à la place de % dans les
# dépendances
# La commande pour la compilation revient à exécuter le fichier md (qui
# est en fait un script) et piper le contenu dans la commande stockée
# dans la variable mk avec pour argument la cible
# make substitue la variable $@ avec le chemin de la cible
root/%.html     : src/%.md page lib/htmlhardcoded ; STYLE=/style.css $< | ${mk} $@
root/%          : src/%; install -D $< $@
root/%.md       : src/%.md; install -D $< $@
root/events.atom : src/events/2022; $< | ${mk} $@
page            : lib/html; touch $@
lib/html        : rc/html; < $< bin/pug2cat | ${mk} $@
