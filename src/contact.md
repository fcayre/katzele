#! page
%T interagir avec Katzele
%A katzele
%S main

Pour nous questionner ou nous solliciter directement, vous pouvez
[nous écrire](mailto:katzele(hahaha!!!)framalistes.org).

Toutefois, les membres de Katzele sont impliqués dans des réseaux que nous vous
invitons à rejoindre puisque nous ne sommes qu'une goute d'eau dans un océan de
d'échanges d'idées:

Ainsi:

Pour nos activités locales, nous communiquons via les
[listes de diffusion du "Libre Users Group alsacien"](https://strasbourg.linuxfr.org/listes/index) (la Flamekuche Connexion). Nous sommes principalement actifs sur lug, linux et code.

Pour nourrir notre réflexion, nous échangeons principalement avec
les réseaux du monde universitaire ([Eveille](https://eveille.hypotheses.org/)
et [Ecoinfo](https://ecoinfo.cnrs.fr/)).

* [le forum ecoinfo](https://listes.services.cnrs.fr/wws/info/ecoinfo-forum-public)
* [Écosophie du numérique](https://groupes.renater.fr/sympa/info/ecosophie-du-numerique)
* [convivialisme numérique](https://groupes.renater.fr/sympa/info/convivialisme-numerique)

Nous sommes aussi en contact avec le collectif
[https://www.adrastia.org/](Adrastia) et contribuons modestement aux
communautés [suckless.org](http://suckless.org) et
[bitreich](gopher://bitreich.org).
