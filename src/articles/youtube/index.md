#! page

%T Guide de survie en territoire youtubesque
%A Arthur Pons
%P 2022-10-19
%S main

# S'affranchir des interfaces de youtube.com en 5 étapes

## Pourquoi ?

Sur Youtube on trouve de l'excellent contenu.
Malheureusement, Youtube est une entreprise qui a ses torts.

Parmis eux, se trouve celui d'avoir une très mauvaise interface.

Liste non exhaustive, non ordonnée et personnelle des défauts de l'interface
web de youtube :

  * contient trop de fonctionnalités, je veux avoir mes abonnements, les 
    vidéos et éventuellement les métadonnées des vidéos
  * est dispendieuse, à la fois en calcul et en mémoire, essayez la sur un
    ordinateur avec peu de ressources pour vous en convaincre
  * change souvent
  * requiert l'éxecution de javascript pour fonctionner, javascript qui sert
    en partie à collecter des données sur votre personne
  * intègre un ensemble de technique à la limite du [dark pattern] pour vous
    inciter à rester plus longtemps sur le site (recommandations, lecture
    automatique, etc)
  * oublie certaines de vos préférences - comme celle de la résolution par
    défaut - en particulier si vous avez configuré votre navigateur pour 
    supprimer les cookies etc
  * requiert un compte google pour profiter de la fonctionnalité des
    abonnements
  * n'affiche pas systématiquement les nouvelles vidéos dans la file des
    abonnements, d'où l'ajout quelque peu absurde de la cloche
  * utilise un algorithme de recommandation qui, au moins par moment, a un
    fort penchant pour les vidéos incitant à l'énervement, la colère, la
    réaction à chaud et favorisant les vidéos faisant la promotion 
    d'idéologies réactionnaires (comme tous les réseaux sociaux)
  * utilise un lecteur de vidéo offrant relativement peu de contrôle en
    comparaison avec les standards non web (vlc, mpv, etc)
  * affiche des publicités

[Un bon article] sur des soucis environnementaux de l'interface youtube que je
ne soupçonnais pas.

Améliorer l'interface existante ou en utiliser une autre pourrait donc, en
théorie et à des degrés divers :

  * d'utiliser de vieux ordinateurs plus longtemps, bon pour le porte monnaie
    bon pour la planète voir de redéfinir ce qu'est un "ordinateur" (capable de
    lire des vidéos)
  * d'économiser de la bande passante, bon pour le porte monnaie, bon pour la
    planète
  * de mieux préserver votre confidentialité
  * de mieux contrôler le temps que vous passez à regarder des vidéos
  * d'adapter le visionnage de la vidéo à vos besoins spécifiques (vitesse de
    lecture, apparence des sous-titre, ajustement de l'image en fonction de
    votre écran, etc)
  * limiter le nombre de comptes que vous possédez
  * ne pas rater de vidéos de vos créateurices préférées
  * ne pas être exposé·e à de la publicité

## Pour les smartphones

Je connais mal les systèmes d'exploitation de smartphones. Je les trouve,
au moins au premier abord, trop peu accueillant à la bidouille pour avoir envie
de consacrer du temps. Cependant puisqu'une majorité du temps passé sur Youtube
l'est à travers d'un [smartphone] il semble utile que je donne au conseil.

J'utilise [NewPipe], une application libre qui offre à peu près les mêmes
fonctionnalités que l'on verra dans la partie PC à travers une interface
similaire à l'application android youtube officielle. Elle permet de s'abonner
sans compte, de choisir une résolution par défaut, de télécharger les vidéos,
de n'avoir que l'audio, de vérouiller l'écran du téléphone sans interrompre la
lecture etc.

Hormis quelque rare bugs et crashs cette application est géniale, je n'envisage
pas de regarder une vidéo youtube autrement sur un téléphone android. Il en
existe un fork (une version alternative) nommée [SkipTube] qui intègre
SponsorBlock (on en reparlera).

Je n'utilise pas iOS, je ne sais pas s'il existe un équivalent. Si vous avez
connaissance d'une application similaire dîtes le moi. J'imagine qu'il est
possible de bidouiller quelque chose de similaire à ce que l'on va voir dans la
partie PC grâce à [Termux] mais je n'ai pas essayé.

## Pour les ordinateurs

Les ordinateurs sont en moyenne des machines avec des systèmes plus ouverts.
Il est généralement plus facile d'écrire ou de modifier les logiciels faits
pour fonctionner sur des PC. C'est en partie pour cela que l'on trouve une
diversité d'option plus large quand il s'agit de modifier ou de remplacer
l'interface web de youtube.

En cinq étapes nous irons progressivement de modifications subtiles et 
"simples" à un système nous affranchissant entièrement de l'interface de
youtube. Vous pourrez ainsi vous arrêter là où vous le souhaitez selon votre
besoin et votre niveau de confort avec les outils que l'on utilisera. Vous
pourrez également piocher dans les différentes étapes pour les combiner et en
faire le système qui vous convient le mieux. Certaines astuces en rendent
d'autres inutiles, je vous invite à les tester.

### 1. Extensions de navigateur

Il existe tout un tas d'extensions faciles à installer dans votre navigateur
qui permettront d'améliorer votre navigation sur youtube. Ci-dessous une liste
non exhaustive et pour Firefox :

[Ublock Origin]

Meilleur bloqueur de pub.

[Enhancer for youtube]

Si j'ai bien fait le tour de l'extension c'est surtout pour améliorer le player
youtube et inscrire dans le marbre quelques paramètres défauts. J'y vois un
intérêt notamment pour figer la résolution.

[Sponsorblock]

Sponsorblock est un base référençant le contenu sponsorisé dans les vidéos.
Elle est alimentée par les utilisateurices de youtube. Cette extension ajoute
des éléments à l'interface de youtube permettant de sauter le contenu
sponsorisé si vous le souhaitez. Par ailleurs la base offre une api si vous
voulez consommer ces infos pour autre chose.

[Youtube recommended videos]

Cette extension permet de dissimuler certains éléments de l'interface dont les
recommandations, les commentaires etc.

[Youtube addon]

Pas essayé.

**Attention**, les extensions peuvent prendre une place significative à
l'installation et consommer une quantité certaine de cycles CPU et de RAM.  Si
vous utilisez une machine peu puissante faites attention de peser les arguments
pour et contre de l'utilisation d'extension notamment si vous utilisez une
machine peu puissante.

### 2. Lecteur RSS

Les utilisateurices de youtube utilisent souvent la fonctionnalité
d'abonnement. Celle-ci permet, sur une page spécifique de l'interface, d'avoir
la liste chronologique des dernières sorties des chaînes auxquelles on est
abonné. Alternativement, il est possible de demander à youtube d'envoyer un
mail.

Ce fonctionnement a ses limites. D'abord, il est bien connu que la liste n'est
pas fiable. C'est pourquoi youtube a ajouté la fonctionnalité de la "cloche"
qui permet de garantir que l'on sera notifié. Deuxièmement, au même titre que
le reste de l'interface, la page d'abonnement est lourde et chargera très
lentement sur un ordinateur peu puissant. Finalement, elle ne propose aucune
personnalisation. Il n'est pas possible de choisir ce qui s'affiche comme info,
combien de vidéos par page, de créer des catégories de chaînes etc.

Une partie de ces limites peuvent être résolues par les extensions vues
précédemment mais une autre solution est possible. Pour notre plus grand
plaisir Youtube maintient des flux [RSS] pour chacune des chaînes.
Techniquement ce sont des flux Atom mais nous considérerons que ces deux types
de flux sont équivalents. 

Si vous savez déjà ce qu'est un flux RSS vous pouvez sauter jusqu'au prochain
titre.

Un flux RSS est un fichier au format XML qui renseigne un ensemble de contenu,
typiquement les dernières mise à jour d'un site tel qu'un blog ou un journal.
Ainsi quand un·e youtubeureuse publie une nouvelle vidéo, le fichier RSS de la
chaîne est mis à jour de façon à déclarer ce nouveau contenu. En consultant
périodiquement le contenu de ce fichier on peut être tenu au courant des
dernières vidéos. Par exemple, le flux RSS de la chaîne Heu?reka se trouve à
cette url :

	https://www.youtube.com/feeds/videos.xml?channel_id=UC7sXGI8p8PvKosLWagkK9wQ

On y trouve plusieurs "entrées", à savoir plusieurs vidéos, et leurs
informations respectives. On y retrouve le titre de la vidéo, la date de
publication, le lien vers la vidéo mais aussi la description qui apparait
habituellement en dessous, le nombre de vues etc. Il serait techniquement
possible de consulter "à la main" ces fichiers pour être à jour mais ça n'est
pas très pratique pour un humain, notamment si vous voulez vous abonner à un
grand nombre de chaîne. Pour vous en convaincre voici ce à quoi ça ressemble :

	<entry>
	 <id>yt:video:WqDVmluGdec</id>
	 <yt:videoId>WqDVmluGdec</yt:videoId>
	 <yt:channelId>UC7sXGI8p8PvKosLWagkK9wQ</yt:channelId>
	 <title>Immobilier &amp; inflation : l'inflation est-elle sous estimée ?</title>
	 <link rel="alternate" href="https://www.youtube.com/watch?v=WqDVmluGdec"/>
	 <author>
	  <name>Heu?reka</name>
	  <uri>https://www.youtube.com/channel/UC7sXGI8p8PvKosLWagkK9wQ</uri>
	 </author>
	 <published>2022-10-17T07:12:08+00:00</published>
	 <updated>2022-10-17T14:37:15+00:00</updated>
	 ...

Il existe des logiciels spécialisés pour agréger et consulter des flux RSS. Ils
sont généralement appelés "Lecteurs RSS". Il existe une très grande diversité
de [lecteurs RSS]. Je prendrais pour exemple Thunderbird puisque c'est un
outil que je connais déjà, que beaucoup de gens utilisent pour leurs mails,
qu'il est libre et pas une application web.

Dans Thunderbird vous pouvez aller dans

	Fichier -> Nouveau -> Compte de flux

Pour créer un "compte" qui regroupera tout vos flux au même titre que vous avez
un ou des "comptes" pour vos différentes adresses mails. Vous pouvez aussi
utiliser le raccourci (en maintenant alt) :

	alt+f+n+d

Une boite de dialogue s'ouvre où vous pouvez choisir le nom du compte. Si l'on
ne veut y mettre que des chaînes youtube on peut choisir un nom similaire à
"flux Youtube". L'important est que vous puissiez le reconnaître rapidement
quand il s'affichera dans la barre Thunderbird à gauche.

Une fois créé, vous pouvez cliquer sur "Gérer les abonnements" pour en ajouter.
La boite de dialogue qui suit vous invite à rentrer l'url du flux, configurer
la fréquence à laquelle Thunderbird va vérifier si de nouvelles vidéos sont
sorties, l'emplacement Thunderbird du flux etc.

Une fois les paramètres rentrés, vous pouvez cliquer sur "Ajouter" et
Thunderbird ira récupérer un historique des dernières vidéos. Chaque message
contiendra le titre de la vidéo en "sujet" et le lien vers la vidéo dans un
champ "site web".  Malheureusement je ne sais pas comment faire en sorte que
Thunderbird affiche la description de la vidéo dans le corps du message.

Si vous avez beaucoup d'abonnements et souhaitez recréer un style de flux comme
celui de l'interface web de youtube vous pouvez ajouter un premier flux,
renommer le dossier qui s'est créé à gauche - qui devrait avoir le nom de la
chaîne que vous venez d'ajouter - par "Youtube" en faisant 

	clique droit -> renommer

puis, lors de l'ajout des futurs flux, choisir dans la liste déroulante le
dossier "Youtube". Si tout est bien paramétré, la boite de dialogue d'ajout
devrait avoir un champ "Articles stockés dans" avec la valeur
"nom_du_compte/Youtube" et, après avoir cliqué sur ajouté, ce dossier devrait
entremêler les vidéos des différentes chaînes par ordre chronologique.

Vous avez dorénavant dans une application locale et rapide toutes les nouvelles
vidéos de vos chaînes préférées sans avoir recours à un compte Google et sans
avoir à consulter l'interface.

Si vous avez compris ce qu'est un flux RSS, comment récupérer son lien et
comment l'ajouter à Thunderbird, vous devriez pouvoir vous adapter à la plupart
des lecteurs RSS disponibles. Les lecteurs ont tous leurs avantages et
inconvénients, si vous n'êtes pas satisfaits par Thunderbird je vous incite à
chercher des alternatives. Nous allons en voir une ensemble à la quatrième
étape. Évitez tout de même de payer un lecteur RSS ou pire, de prendre
abonnement à un lecteur RSS. Il existe de nombreux logiciels libres entièrement
gratuits qui font l'affaire.

Il n'y a malheureusement pas de moyen parfaitement fiable d'obtenir le flux rss
d'un site/chaîne/blog. Il existe parfois le logo des flux quelque part sur la
page. Le flux est parfois caché dans le footer des pages. Il est souvent
possible de le trouver dans les sources de la page. Pour cela, sur firefox,
faire `Ctrl+U` pour afficher les sources du site qui vous intéresse puis
cherchez "rss" avec `Ctrl+F` pour peut-être tomber sur le lien du flux.

Sinon l'ubiquité de certains CMS fait que beaucoup de sites ont un flux rss
sans même le savoir, accessible en ajoutant "/rss" ou "/feed" à la fin de l'url
de la page d'accueil.

Il existe un script nommé [sfeed_web] qui prend en entrée une page html et
cherche le ou les flux rss dans la page.

Par exemple sur linux :

	curl -sL lemonde.fr | sfeed_web
	https://www.lemonde.fr/rss/une.xml	application/rss+xml

### 3. Un lecteur de vidéos alternatif

Nous sommes dorénavant débarrassés du compte Google et n'avons plus à
consulter la page d'abonnement des enfers. Pour parfaire nos besoins de
performances il faut également substituer la page de lecture de la vidéo elle
même. Le lecteur web est très peu performant.

Il semblerait que le décodage de la vidéo ne soit pas plus gourmand selon s'il
est fait sur youtube.com ou en accédant à la vidéo directement en la lisant
dans le lecture HTML5 de base. En terme de performance la différence notable se
trouve au temps de chargement de la page.  On peut donc émettre l'hypothèse que
la difficulté du décodage vidéo n'est imputable qu'à firefox et pas youtube.
Les performances que je constate sont similaires sur d'autres navigateurs que
firefox.

Quoi qu'il en soit sur ma machine professionnelle équipée d'un
Intel i5 de septième génération avec quatre cœurs, un simple comparatif de
charge entre le lecteur web et vlc donne les résultats suivant :

	             1080p60fps  1080p30fps  480p  240p  audio
	firefox      70%         48%         32%   26%   -
	vlc          25%         15%         10%   9%    5%
	
	Chaque valeur est en % utilisé de la puissance totale du cpu

Commentaire :

J'ai retesté sur cette vidéo https://www.youtube.com/watch?v=LXb3EKWsInQ.
C'est marrant, en 1080p60 tous les cœurs sont à 100% pour les flux vidéos HDR,
plutôt autour de 55/60% pour les flux non HDR. Constat similaire sur mpv.

Fin de commentaire

Sur un raspberry3 on obtient ces résultats :

	         720p                   480p  240p  audio
	firefox  bloquée sur une image  77%   65%   -
	mpv      40%                    25%   15%   10%

On constate une charge diminuée par un facteur de 3 ou plus ! Autrement dit,
utiliser un vieil/micro ordinateur ne condamne pas à ne pas pouvoir lire de
vidéo. C'est le web qui nous fait croire l'inverse. Vous aviez peut-être déjà
entendu qu'un raspberry "ça tourne bien mais faut pas espérer regarder des
vidéos youtube quoi". En fait si, si l'on se sépare du puits de ressource que
sont les navigateurs.

En plus de cela, le fait de pouvoir fortement personnaliser les lecteurs tels
que vlc ou mpv me fait penser qu'ils sont plus accessibles. N'étant pas en
situation de handicap je ne peux pas confirmer cette intuition, si vous êtes
concerné·e cela m'intéresse.

La liste des avantages fonctionnels de logiciels comme mpv ou vlc sur des
lecteurs de vidéo web est très longue. Deux exemples qui me concernent
directement. Je regarde pas mal de directs, sur youtube ou twitch.
Les regarder dans mpv permet de remonter dans le temps. Vous avez vu ou entendu
quelque chose que vous voulez revoir tout de suite mais personne n'a "clippé"
ce passage ? Le replay n'a pas choisi la bonne action ? Pas d'inquiétude, les
lecteurs gardent une partie de la vidéo en cache pour que vous puissiez revenir
en arrière et le regarder au rythme ou vous voulez.
Maintenant vous voulez aller aux toilettes et vous regardez autre chose qu'un
match de foot qui pourrait vous être spoilé par vos voisins ? Faîtes pause et
regardez le reste de la vidéo en revenant. Vous aimeriez tout de même revenir
au direct sans rien rater ? Accélérez la lecture de la vidéo et laissez faire.

Cela dit, comment faire pour lire une vidéo youtube en dehors du navigateur ?
Il y a plusieurs approches et plusieurs outils, nous allons nous intéresser à
chacune des briques pour ensuite pouvoir les combiner selon notre souhait.

#### yt-dlp + mpv

Nous allons utiliser le lecteur [mpv]. C'est une sorte d'équivalent à VLC qui a
la particularité d'être facile à utiliser via les lignes de commande. Sur un
fichier vidéo locale mpv s'utilise aussi simplement que :

	mpv video.mp4

Comme tous les lecteurs vidéos dignes de ce nom mpv peut aussi lire un flux
vidéo streamé. Pour cela il suffit de lui passer en argument le lien vers ce
flux à la place du nom du fichier. Par exemple, la commande suivante ouvrira
une vidéo de démo du logiciel "aerc" habituellement présentée sur
https://aerc-mail.org/ :

	mpv https://yukari.sr.ht/aerc-intro.webm

Si l'on fait la même chose pour une vidéo youtube :

	mpv https://www.youtube.com/watch?v=dQw4w9WgXcQ

La vidéo s'ouvre bien avec mpv. Cependant vous noterez ce qui s'affiche dans
votre console :

	 (+) Video --vid=1 (*) (h264 1920x1080 25.000fps)
	 (+) Audio --aid=1 --alang=eng (*) (opus 2ch 48000Hz)
	AO: [pulse] 48000Hz stereo 2ch float
	VO: [gpu] 1920x1080 yuv420p

La vidéo a automatiquement été lancée en 1080p. Par défaut mpv choisi la
résolution disponible la plus élevée. Même si cela peut parfaitement convenir
à votre besoin il serait préférable d'avoir un peu plus de contrôle, par choix
mais aussi par nécessité. Si vous exécutez cette commande par exemple :

	mpv https://www.youtube.com/embed/LrzWrvOjJm8

vous risqueriez d'avoir des surprises. La plus haute résolution disponible pour
cette vidéo est la 8k. J'espère pour vous que vous avez une bonne bande
passante et une machine de guerre. Pour nous permettre de choisir la résolution
nous allons faire appel à un logiciel nommé [yt-dlp], un fork activement
développé de youtube-dl.

Ce logiciel permet, entre mille autres choses, d'afficher et de sélectionner
les différentes qualités d'une vidéo youtube. Il fonctionne pour de nombreux
autres sites d'ailleurs. Avec l'option `-F` nous pouvons afficher les
résolutions disponibles pour cette vidéo du débat présidentiel sur le climat :

	yt-dlp -F 1-28-grEZjk
	[youtube] 1-28-grEZjk: Downloading webpage
	[youtube] 1-28-grEZjk: Downloading android player API JSON
	[youtube] 1-28-grEZjk: Downloading player c6736352
	[info] Available formats for 1-28-grEZjk:
	ID  EXT   RESOLUTION FPS |   FILESIZE   TBR PROTO | VCODEC        VBR ACODEC      ABR     ASR MORE INFO
	------------------------------------------------------------------------------------------------------------------------
	sb2 mhtml 48x27          |                  mhtml | images                                    storyboard
	sb1 mhtml 80x45          |                  mhtml | images                                    storyboard
	sb0 mhtml 160x90         |                  mhtml | images                                    storyboard
	139 m4a                  |   67.62MiB   48k https | audio only        mp4a.40.5   48k 22050Hz low, m4a_dash
	249 webm                 |   70.80MiB   51k https | audio only        opus        51k 48000Hz low, webm_dash
	250 webm                 |   88.68MiB   63k https | audio only        opus        63k 48000Hz low, webm_dash
	140 m4a                  |  179.47MiB  129k https | audio only        mp4a.40.2  129k 44100Hz medium, m4a_dash
	251 webm                 |  159.61MiB  115k https | audio only        opus       115k 48000Hz medium, webm_dash
	17  3gp   176x144      7 |  105.09MiB   75k https | mp4v.20.3     75k mp4a.40.2    0k 22050Hz 144p
	160 mp4   256x144     30 |   79.39MiB   57k https | avc1.4d400c   57k video only              144p, mp4_dash
	278 webm  256x144     30 |   95.95MiB   69k https | vp9           69k video only              144p, webm_dash
	133 mp4   426x240     30 |  161.61MiB  116k https | avc1.4d4015  116k video only              240p, mp4_dash
	242 webm  426x240     30 |  161.06MiB  116k https | vp9          116k video only              240p, webm_dash
	134 mp4   640x360     30 |  295.99MiB  213k https | avc1.4d401e  213k video only              360p, mp4_dash
	18  mp4   640x360     30 |  760.45MiB  548k https | avc1.42001E  548k mp4a.40.2    0k 44100Hz 360p
	243 webm  640x360     30 |  413.92MiB  298k https | vp9          298k video only              360p, webm_dash
	135 mp4   854x480     30 |  472.75MiB  341k https | avc1.4d401f  341k video only              480p, mp4_dash
	244 webm  854x480     30 |  574.87MiB  414k https | vp9          414k video only              480p, webm_dash
	136 mp4   1280x720    30 |    1.47GiB 1084k https | avc1.4d401f 1084k video only              720p, THROTTLED, mp4_dash
	247 webm  1280x720    30 |    1.02GiB  755k https | vp9          755k video only              720p, THROTTLED, webm_dash
	22  mp4   1280x720    30 | ~  1.68GiB 1213k https | avc1.64001F 1213k mp4a.40.2    0k 44100Hz 720p
	298 mp4   1280x720    60 |    2.60GiB 1923k https | avc1.4d4020 1923k video only              720p60, mp4_dash
	302 webm  1280x720    60 |    2.32GiB 1715k https | vp9         1715k video only              720p60, webm_dash
	299 mp4   1920x1080   60 |    5.05GiB 3731k https | avc1.64002a 3731k video only              1080p60, mp4_dash
	303 webm  1920x1080   60 |    4.35GiB 3213k https | vp9         3213k video only              1080p60, webm_dash

Si on passe un peu de temps à analyser ce tableau nous pouvons en apprendre
beaucoup sur le fonctionnement de youtube. On constate que youtube a des
streams audio (ID de 139 à 251), des streams vidéos (tous ceux avec "video
only" dans la colonne ACODEC - audio codec) et deux streams avec les deux
ensemble (ID 18 et 22). A l'aide d'algo un peu malin et du protocole [HLS]
youtube peut identifier la combinaison de streams audio et vidéo qui conviendra
le mieux à votre débit - mais pas la puissance de votre machine ou alors c'est
très mal implémenté - et s'adaptera en temps réel.

En ayant connaissance de ces identifiants et à l'aide de l'option `-f` de
yt-dlp nous pouvons dorénavant choisir précisément les streams que l'on veut.
C'est le moment de faire son marché. Puisque c'est un débat présidentiel, nous
décidons de ne récupérer que l'audio, comme nous le ferions pour un podcast.
Mais attention, si nous lancions telle quelle la commande :

	yt-dlp -f 139 https://www.youtube.com/watch\?v\=1-28-grEZjk

nous téléchargerions en local la totalité du débat (67Mo). Pour pouvoir le
streamer dans mpv il nous faut ajouter une option `--get-url` qui permet
d'obtenir le lien direct vers ce stream audio. C'est ensuite ce lien que l'on
fera passer en argument à mpv :

	yt-dlp -f 139 --get-url https://www.youtube.com/watch\?v\=1-28-grEZjk
	https://rr1---sn-4g5e6ns6.googlevideo.com/videoplayback?expire=1648249604&ei=pPY9YtiPB837xN8PiuiKsAU&ip=2a01%3Acb10%3A2da%3A2600%3Ae488%3A3768%3A858%3Afc0d&id=o-AB05vEVWFOI_-gi5O6CK_NLkCWMx0X_CVYKxDabR7WTp&itag=139&source=youtube&requiressl=yes&mh=pM&mm=31%2C26&mn=sn-4g5e6ns6%2Csn-25ge7nzs&ms=au%2Conr&mv=m&mvi=1&pl=40&initcwndbps=1018750&vprv=1&mime=audio%2Fmp4&gir=yes&clen=70904512&dur=11627.809&lmt=1647388719857621&mt=1648227660&fvip=1&keepalive=yes&fexp=24001373%2C24007246&beids=23886207&c=ANDROID&txp=5432434&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRQIgdV_eYJ9F1UehezBlnjURICGL7dkKjdz2NPCya6b7tWICIQCMzNCuNNZrg9YE04omDXHQZ6zr0xiYyS2mhKtYA9ZihA%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRQIgdGhwPJPgf4QTOD6i3hiDPURnvoNBfc0kMcxY_IKY5VoCIQDB-B35CY6KlUPI0FmhyjFww9nKfwnsUkOHVaWMToHtdA%3D%3D

C'est derrière ce lien barbare que se cache le stream. Si vous avez exécuté la
commande vous pouvez essayer d'ouvrir le lien récupéré dans un navigateur, cela
fonctionnera parfaitement bien. Celui dans l'article est expiré, youtube les
change constamment. Pour éviter d'avoir à le copier/coller vous pouvez capturer
le résultat de la commande pour la faire passer en argument à MPV :

	mpv $(yt-dlp -f 139 --get-url https://www.youtube.com/watch\?v\=1-28-grEZjk)

Les formats de yt-dlp peuvent être conditionnels, porter sur la taille des
fichiers, le bitrate etc. Par exemple on peut lui demander le pire stream audio
avec le format `worstaudio` ou le flux vidéo le plus proche d'un certain
bitrate. Voir la [documentation] pour plus d'infos.

Une liste non exhaustive des options de `yt-dlp` que je trouve utiles :

  * `-F` permet de lister les formats disponibles
  * `-f` permet de choisir le format
  * `--get-url` renvoie l'url directe vers la vidéo au bon format plutôt que de
    lancer le téléchargement
  * `-U` met à jour yt-dlp
  * `--write-subs` extrait les sous-titres disponibles

Encore plus simple, mpv vient avec une option qui, si vous avez yt-dlp
installé sur votre pc, vous permet de faire passer des formats dans la même
commande. Par exemple, si l'on veut un son d'une bonne qualité et une image un
peu médiocre parce que l'on fait à manger en même temps et que l'on y jettera
un coup d'oeil que rarement :

	mpv --ytdl-format="251+134" https://www.youtube.com/watch\?v\=1-28-grEZjk

mpv se chargera de faire appel à `yt-dlp` lui même.

Liste non exhaustive d'options de `mpv` que je trouve utiles :

  * `--ytdl-format` pour choisir le format de la vidéo
  * `--volume` pour choisir le volume au démarrage de la vidéo
  * `--cache-secs` pour déterminer le nombre de secondes pré-chargées
  * `--term-osd-bar` pour afficher une barre de progression dans la console,
    pour un podcast par exemple

TODO : Ajouter un passage sur --script-opts=ytdl_hook-all-formats=yes et --hls-bitrate si ça fonctionne
TODO : prérequis, créer une issue sur github ?

A ce stade là vous pourriez vous faire la remarque : "Génial mais c'est
fatiguant de taper la commande à chaque fois, de faire des copier/coller etc et
qu'est-ce que l'on fait si l'on veut faire une recherche ?" Et vous auriez
raison !

Nous allons voir trois solutions, l'une pour les liens youtube sur lesquels
vous tomberez inévitablement en parcourant le web, l'autre qui ne dispensera
pas de copier/coller mais facilitera l'affaire si quelqu'un vous envoie un lien
youtube via une application de messagerie par exemple, la dernière qui vous
permettra de chercher une vidéo youtube sans passer par youtube.com.

#### ff2mpv

[ff2mpv] est une extension chrome ou firefox permettant de lancer la lecture
d'une vidéo youtube depuis le navigateur dans mpv. Elle fonctionne en
installant sur votre machine un script qui lancera une commande identique à
celle que l'on a construit précédemment. Ce script se lancera quand vous ferez
clique droit sur un lien youtube puis "Play in MPV".

La procédure d'installation est disponible sur [le wiki du dépôt github] de
l'extension.

Le nerf de la guerre se trouve dans un script nommé `ff2mpv.py` qui a pour but de
lancer une commande similaire à celles que l'on a construit plus tôt dans
l'article.

Dans mon ff2mpv.py j'ai par exemple :

	args = ["mpv","--ytdl-format=worstvideo[height>=400]+worstaudio/worstvideo[height>=300]+worstaudio/worstvideo[height>=700]+worstaudio/worst/best","--cache-secs=15","--",url]

Cette commande utilise les formats conditionnels de yt-dlp pour tenter d'avoir,
dans cet ordre précisément, la meilleure qualité en 480p, sinon en 360p sinon
en 720p sinon la meilleure qualité tout court.

#### alias

Si vous utilisez une distribution linux vous pouvez créer des alias. Un alias
permet d'exécuter une commande en en tapant une autre généralement plus courte.
C'est une sorte de raccourci. Nous pourrions par exemple créer l'alias 

	yt() mpv --ytdl-format="18" --cache-secs=15 --volume=50 "$@"

Grâce à cet alias, si vous avez actuellement dans le presse-papier un lien
youtube il suffit de taper

	yt ctrl+v

pour lancer une vidéo youtube dans mpv avec les options que vous conviennent.
Ici j'ai choisi le format `18`, vidéo et audio muxés en 360p.

#### Idiotbox pour la recherche youtube

Vous voulez retrouver ce super meme qui vous avait bien fait rire ? Vous ne
voulez pas avoir affaire à l'interface youtube.com ? Quelqu'un a créé un petit
logiciel nommé [idiotbox] exactement pour cela. Il offre une interface web -
et gopher ! - très simple, qui va faire la recherche youtube pour vous et vous
présente les résultats de façon claire et sobre. Pas besoin de l'installer, la
personne l'ayant développé en héberge une [instance] elle même. 

### 4 : Vive la console

Cette étape vise à transposer ce que l'on a vu précemment dans la console en
faisant appel à peu de dépendances. L'idée est d'avoir un ensemble d'outils qui
permet d'avoir une utilisation agréable et efficace de youtube sur un
ordinateur aussi peu puissant qu'un raspberry et avec une quantité de mémoire
limitée. Cela permet également d'avoir uniquement recours au clavier. Deux
nouveaux outils vont nous aider dans ce travail, sfeed et lynx.

#### sfeed et sfeed_curses

[sfeed] est un ensemble d'outils permettant de parser, manipuler, lire des
flux atom/rss. Le nom du projet porte celui de son composant principal, le
programme sfeed, qui fait le gros du travail en parsant l'xml des flux.

Bien qu'sfeed ne soit pas si compliqué que ça je ne vais pas m'aventurer à en
expliquer tout le fonctionnement. Le [README] du projet et les pages de manuel
des scripts fournis sont très bien fait, je vous recommande vivement de les
lire attentivement - et pas que en diagonale en cherchant rapidement ce dont
vous avez besoin, je vous vois !

Je vais expliquer les modifications que j'y ai apporté pour deux raisons.

1. Vous voudriez peut-être faire les mêmes
2. Démontrer que c'est un ensemble d'outil très "hackable", certains diraient
   conviviale

Tout commence avec sfeed. Fondamentalement tout ce qu'sfeed fait est de
transformer des données ayant cette tête - ici les deux premiers articles de la
une du monde tel que décrits dans le flux rss du journal :

    <item>
      <title><![CDATA[Pénurie de carburant, en direct : la grève est reconduite sur les sites de TotalEnergies et Esso-ExxonMobil, la CGT appelle à élargir le mouvement à tout le secteur de l’énergie]]></title>
      <pubDate>Thu, 13 Oct 2022 08:14:13 +0200</pubDate>
      <description><![CDATA[TotalEnergies « doit augmenter ses salaires » et la CGT doit « se saisir de la main qui a été tendue » pour négocier, a affirmé jeudi matin sur RTL, le ministre de l’économie, Bruno Le Maire.]]></description>
      <guid isPermaLink="true">https://www.lemonde.fr/economie/live/2022/10/13/penurie-de-carburant-la-greve-se-poursuit-dans-les-raffineries-premieres-requisitions-chez-esso-exxonmobil-suivez-la-situation-en-direct_6145597_3234.html</guid>
      <link>https://www.lemonde.fr/economie/live/2022/10/13/penurie-de-carburant-la-greve-se-poursuit-dans-les-raffineries-premieres-requisitions-chez-esso-exxonmobil-suivez-la-situation-en-direct_6145597_3234.html</link>
      <media:content url="https://img.lemde.fr/2022/10/13/500/0/6000/3000/644/322/60/0/0a467f5_1665643499826-dsc04333bis.jpg" width="644" height="322">
        <media:credit scheme="urn:ebu">MARIE BENOIST POUR « LE MONDE »</media:credit>
      </media:content>
    </item>
    <item>
      <title><![CDATA[Pap Ndiaye, ministre de l’éducation nationale : « Il y a bel et bien une vague de port de tenues pouvant être considérées comme religieuses »]]></title>
      <pubDate>Thu, 13 Oct 2022 06:55:08 +0200</pubDate>
      <description><![CDATA[Le ministre dévoile les chiffres des atteintes à la laïcité, qui sont en hausse. Un phénomène porté notamment par les réseaux sociaux, assure-t-il.]]></description>
      <guid isPermaLink="true">https://www.lemonde.fr/societe/article/2022/10/13/pap-ndiaye-la-republique-est-plus-forte-que-tiktok_6145585_3224.html</guid>
      <link>https://www.lemonde.fr/societe/article/2022/10/13/pap-ndiaye-la-republique-est-plus-forte-que-tiktok_6145585_3224.html</link>
      <media:content url="https://img.lemde.fr/2022/10/12/1664/0/4438/2219/644/322/60/0/ab91c70_1665584679283-ndiaye-alcock-008.JPG" width="644" height="322">
        <media:description type="plain">Pap Ndiaye, ministre de l'Éducation nationale et de la Jeunesse, au ministère de l'éducation nationale, à Paris, le 11 octobre 2022. </media:description>
        <media:credit scheme="urn:ebu">ED ALCOCK/MYOP POUR « LE MONDE »</media:credit>
      </media:content>
    </item>

en un format TSV - Tab Separated Value :

	1665641653	Pénurie de carburant, en direct : la grève est reconduite sur les sites de TotalEnergies et Esso-ExxonMobil, la CGT appelle à élargir le mouvement à tout le secteur de l’énergie	https://www.lemonde.fr/economie/live/2022/10/13/penurie-de-carburant-la-greve-se-poursuit-dans-les-raffineries-premieres-requisitions-chez-esso-exxonmobil-suivez-la-situation-en-direct_6145597_3234.html	TotalEnergies « doit augmenter ses salaires » et la CGT doit « se saisir de la main qui a été tendue » pour négocier, a affirmé jeudi matin sur RTL, le ministre de l’économie, Bruno Le Maire.	html	https://www.lemonde.fr/economie/live/2022/10/13/penurie-de-carburant-la-greve-se-poursuit-dans-les-raffineries-premieres-requisitions-chez-esso-exxonmobil-suivez-la-situation-en-direct_6145597_3234.html			
	1665636908	Pap Ndiaye, ministre de l’éducation nationale : « Il y a bel et bien une vague de port de tenues pouvant être considérées comme religieuses »	https://www.lemonde.fr/societe/article/2022/10/13/pap-ndiaye-la-republique-est-plus-forte-que-tiktok_6145585_3224.html	Le ministre dévoile les chiffres des atteintes à la laïcité, qui sont en hausse. Un phénomène porté notamment par les réseaux sociaux, assure-t-il.	html	https://www.lemonde.fr/societe/article/2022/10/13/pap-ndiaye-la-republique-est-plus-forte-que-tiktok_6145585_3224.html			

L'avantage de ce format est qu'il est bien plus facile de le manipuler,
notamment avec tous les outils standards Unix.

On pourrait requêter à la main le flux rss et le faire passer par sfeed à
chaque fois que l'on veut se mettre à jour mais ce ne serait pas très pratique.
Le projet sfeed prévoit un mécanisme de mise à jour nommé `sfeed_update`. Ce
script a pour but de prendre une liste de flux auxquels vous êtes abonné·e et
écrire les fichiers correspondants au format sfeed dans le dossier adéquat -
normalement `~/.sfeed/feeds`. Il est configurable via le fichier de conf
`~/.sfeed/sfeedrc`.

sfeedrc est un fichier de "configuration" mais en réalité c'est un script shell
qui surcharge la fonction `feeds` qui se trouve dans le script `sfeed_update`.
La surcharge permet d'appeler la fonction `feed` de `sfeed_update` sur tous les
flux. S'abonner à un flux revient donc à ajouter une ligne type

	feed 'Le Réveilleur' 'https://www.youtube.com/feeds/videos.xml?channel_id=UC1EacOJoqsKaYxaDomTCTEQ'

à la fonction `feed` dans sfeedrc. Le premier argument sera le nom du fichier
créé qui contiendra la sortie sfeed appelé sur le lien vers le flux.

Après avoir listé toutes les chaînes auxquelles vous voulez vous abonner il
suffit d'exécuter `sfeed_update` pour générer tous les fichiers. Il devrait y
avoir dans `~/.sfeed/feeds` des fichiers texte au format TSV.

Il est assez évident que s'il n'est pas agréable de lire un flux rss en xml, ce
n'est pas beaucoup mieux au format sfeed tel quel. Le projet vient donc avec un
ensemble de script qui permettent de formatter la sortie d'sfeed. Par exemple
`sfeed_html` transforme le flux en fichier html, `sfeed_plain` en un format
texte relativement lisible pour un humain etc.

L'interface fournie la plus aboutie est probablement sfeed_curses. C'est un
logiciel, basé sur la librairie curses, qui génère une interface TUI - Terminal
User Interface - permettant de parcourir ses flux et ouvrir les éléments.

Si vous avez réussi à exécuter sfeed_update une première fois et que vous faites

	sfeed_curses ~/.sfeed/feeds/*

l'interface devrait charger l'ensemble des flux. S'affichera à gauche un menu
permettant de parcourir les flux. En un sélectionnant vous verrez s'afficher à
droite tous les éléments du flux.

Il faut encore choisir comment vous allez ouvrir le contenu. Par défaut
`sfeed_curses` utilise le paramétrage de `xdg-open` et ouvrira très
probablement les liens dans votre navigateur de choix. Vous pouvez conserver ce
fonctionnement ou le modifier. `sfeed_curses` laisse la possibilité de donner
une valeur à une variable d'environnement `SFEED_PLUMBER` qui devra pointer
vers un script. `sfeed_curses` exécutera ce script en lui passant en argument
le lien de l'élément que l'on veut ouvrir. On peut ainsi dans le script choisir
très exactement avec quoi nous voulons ouvrir l'élément, selon si c'est un
article, une image, une vidéo etc. Si vous voulez que le contenu s'ouvre dans
la même fenêtre il faut aussi donner la valeur "1" à la variable
`SFEED_PLUMBER_INTERACTIVE`.

Pour modifier la valeur des variables d'environnement

	export SFEED_PLUMBER=~/.sfeed/sfeed_plumber
	export SFEED_PLUMBER_INTERACTIVE="1"

Evidemment le script `sfeed_plumber` peut se nommer n'importe comment et se
trouver où vous voulez.

A la lumière de tout ce que l'on a vu précedemment, je recommande d'ouvrir les
vidéos youtube avec mpv, les articles avec lynx etc. Pour exemple mon script
`sfeed_plumber` ressemble à quelque chose comme ça :

	#!/bin/sh
	
	case $1 in
		*"youtube.com/watch"* )
			[ "$2" = 'audio' ] && format="worstaudio" || format="18"
			mpv --ytdl-format="$format" --cache-secs=30 --volume=50 "$1"
			;;
		*"tube.picasoft.net"* )
			mpv --ytdl-format="480p" --cache-secs=15 --volume=50 "$1"
			;;
		*"twitch.tv"* )
			mpv --ytdl-format="480p" --volume=50 "$1"
			;;
		*".mp4"* )
			mpv "$1"
			;;
		* )
			lynx "$1"
	esac

Dernière modification que je vais couvrir mais qui m'est particulièrement
personnelle. Je suis abonné a beaucoup de chaînes. Mon `sfeed_update` génère
donc beaucoup de fichiers dans `~/.sfeed/feeds`. En les ouvrant dans
`sfeed_curses` je dois donc parcourir une longue liste sur la gauche pour
vérifier quelles chaînes ont publié récemment. Je préfère avoir une liste
unique comme sur l'interface youtube. Pour cela il faut que je concatène tous
les flux youtube en un seul fichier au format sfeed, trié chronologiquement. 

J'ai modifié `sfeedrc` pour que les fichiers générés depuis des flux youtube
contiennent tous `[Y]` dans leurs noms. Exemple :

	feed '[Y] Le Réveilleur' 'https://www.youtube.com/feeds/videos.xml?channel_id=UC1EacOJoqsKaYxaDomTCTEQ'

J'ai ensuite ajouté ce bout de shell à la fin de la fonction `feed` - écrit y'a
longtemps, j'pense qu'on peut mieux faire - pour faire la concaténation.

	wait
	cd ~/.sfeed/feeds
	twoweeksago=$(date --date="2 weeks ago" +%s)
	cat \[Y\]\ * | awk -v t=$twoweeksago '$1 > t' | sort -t '	' -k1rn,1 > youtube
	rm -f \[Y\]\ *

J'obtiens ainsi un seul fichier youtube qui décrit, dans l'ordre
antéchronologique, toutes les vidéos sorties sur les deux dernières semaines.

Je peux finalement faire

	sfeed_curses < ~/.sfeed/feeds/youtube

pour ouvrir mon flux d'abonnement youtube.
Si vous voulez pas retaper cette commande souvenez vous des alias.

Si vous avez suivi jusque là vous remarquerez qu'`sfeed_curses` n'affiche pas
les noms de chaînes et les titres mais la date de publication de l'élément.

Pour y remédier j'ai patché les sources afin que s'affiche dans la première
colonne non pas le nom du fichier mais la valeur que l'on trouve dans le champ
"auteur" du flux rss. Vous pouvez trouver le patch
[ici](./display_author.patch).

Il est appliquable avec la commande `patch -p1 < fichier_du_patch`.

[Autre patch](./arguments-to-plumber.patch) pour permettre de passer un
argument supplémentaire au script `sfeed_plumber`, en plus du lien à ouvrir. En
l'occurence je l'ai utilisé pour que le raccourci `o` ne fasse plus strictement
la même chose que d'appuyer sur entrée mais à la place passe en argument une
chaîne de caractère "audio". Je vérifie ensuite dans `sfeed_plumber` la valeur
de cette chaîne et choisi le format à lancer en conséquence.

J'ai fait un semestre de C en cours en 2013 quand j'avais 17 ans. J'ai pourtant
été capable d'effectuer ces modifications relativement facilement. C'est une
démonstration de plus qu'un logiciel simple, modeste dans ses fonctionnalités
et bien compartimenté peut être réappropriable pour un·e informaticien·ne très
moyen·ne même s'il est développé en C. Pas besoin de documentation, pas besoin
d'un système d'extension complexe, pas besoin d'un panneau de configuration.
Vous voulez modifier quelque chose, modifiez le code. Vous n'avez *jamais* codé
? Demandez à votre geek préféré·e et il y arrivera peut-être ou cherchez sur
internet des patchs simples à appliquer. Dans tous les cas on ne complexifie
pas le socle, il se doit d'être maintenable et simple à comprendre.

Je détail la procédure que j'ai suivi pour la première modification pour que
vous puissiez juger de vous même de la difficulté de l'opération (et peut-être
penser que j'abuse totalement en disant que c'est pas si difficile).

Pour ce patch j'ai ouvert `sfeed_curses.c` dans vim. J'ai jeté un coup d'oeil
aux structures de données, j'ai repéré `struct item` ligne 121.  Cette
structure a un attribut `char *fields`. J'ai effectué une recherche dessus - en
me plaçant dessus et appuyant sur `*` de façon répétée - pour tomber sur
`fields[FieldUnixTimestamp]` ligne 1132. J'en déduis qu'il doit exister quelque
part la liste des champs d'un item. Je me place au début du mot, le copie - en
faisant `yw`- écrit sur la ligne suivante la commande - en collant le nom du
champ avec `p`:

	grep -n "FieldUnixTimestamp" ./*

Je récupère le résultat de la commande en l'exécutant avec `.!sh`. J'obtiens

	./sfeed_atom.c:79:		if (strtotime(fields[FieldUnixTimestamp], &parsedtime) ||
	./sfeed_curses.c:1137:	if (!strtotime(fields[FieldUnixTimestamp], &parsedtime)) {
	./sfeed_curses.c:1275:			if (!strtotime(fields[FieldUnixTimestamp], &parsedtime))
	./sfeed_frames.c:44:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	./sfeed_gopher.c:89:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	./sfeed_html.c:44:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	./sfeed_mbox.c:77:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	./sfeed_mbox.c:89:		       fields[FieldUnixTimestamp],
	./sfeed_mbox.c:90:		       fields[FieldUnixTimestamp][0] ? "." : "",
	./sfeed_plain.c:29:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	./sfeed_twtxt.c:27:		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
	grep: ./themes: Is a directory
	./util.h:55:	FieldUnixTimestamp = 0, FieldTitle, FieldLink, FieldContent,

Je vois qu'il existe une liste des champs dans `util.h` à la ligne 55. Je me
place sur le nom du fichier et je l'ouvre avec `Ctrl+W F` et me rends à la
ligne 55 avec la commande `:55`. Je constate l'existence d'un champ
`FieldAuthor` qui semble être ce dont j'ai besoin. Je ferme le ficher avec
`:q`. Je supprime les lignes que j'avais généré en faisant `u`. Puisque l'on
sait que la fonction que l'on veut modifier affiche déjà le titre de l'élément
on continue notre recherche sur `FieldTitle`. On tombe tout de suite sur la
fonction `item_row_format` ligne 1807. Ligne 1824 si l'élément est daté (c'est
plus ou moins toujours le cas) alors la fonction imprime la date et le titre de
l'élément. Nous supprimons les lignes 1827 et 1828 pour afficher à la place
l'auteur·ice et le titre. Il faut également modifier le troisième argument de
la fonction `snprintf` ligne 1825 pour lui dire que les éléments à afficher
sont une chaîne de caractère dont on limite la taille à 25 caractères (choix
personnel) et une seconde chaîne de caractère sans limite de taille et non plus
des entiers. Finalement je modifie le calcul de `needsize` ligne 1818 pour
inclure la longeur du nom de l'auteurice.

Au passage, je sais pas vraiment ce que je fais et je crois que cette modif
introduit un très léger bug d'affichage du titre du premier élément dans la
liste à l'ouverture d'`sfeed_curses`.

Vous n'avez bien sûr aucune obligation d'utiliser `sfeed` comme je le fais.
C'est la beauté de cet ensemble d'outil, il cause très bien avec lui même et
avec le reste des outils Unix. Vous pouvez donc relativement facilement en
faire ce que vous voulez. Le détail de ma configuration est plus là pour faire
la démonstration que c'est accessible à toute personne un peu technique et
facilement "packageable" pour les autres que pour vous convaincre que vous
devriez faire pareil.

#### lynx

Lynx est un navigateur qui tourne dans la console. C'est un logiciel
relativement léger qui permet de rapidement parcourir des pages webs. Du moins
quand elles sont simples et proches de la vision originelle du web. Lynx n'est
pas capable d'exécuter du javascript et ne se base que sur la sémantique des
balises HTML pour produire la mise en page. Autant dire que la grande majorité
des sites webs commerciaux ne fonctionnent pas sur Lynx, en particulier
youtube.

Un simple blog, un site institutionnel "vitrine" ou un outil tel qu'idiotbox
fonctionnera bien. Par exemple, si l'on exécute :

	lynx codemadness.org/idiotbox
	
	                                     Search: "" sorted by relevance
	__________________________________________________________
	Search [Relevance____]

on voit que Lynx nous propose de rentrer un terme de recherche. Un résultat de
recherche aura la tête suivante :

	3blue1brown_______________________________________________
	Search [Relevance____]
	  _____________________________________________________
	
	Oh, wait, actually the best Wordle opener is not “crane”…
	3Blue1Brown | Atom feed
	Published: 1 month ago
	4,122,490 views
	10:53
	  _____________________________________________________
	
	Solving Wordle using information theory
	3Blue1Brown | Atom feed
	Published: 1 month ago
	8,200,917 views
	30:38

Ce qui est autrement plus léger et lisible que l'interface web youtube pour peu
que l'on accepte de ne plus voir les vignettes. Ce qui, j'espère que vous en
conviendrez, est plus une feature qu'un bug. Si vous appuyez sur la touche
entrée pour tenter de consulter la vidéo vous verrez ce charmant message
d'erreur

	Try watching this video on www.youtube.com, or enable JavaScript if it is disabled in your browser.

Il va falloir trouver une parade. Personnellement j'utilise la fonctionnalité
des "externals" de Lynx. Dans le fichier de configuration de lynx - sur ma
machine `/etc/lynx/lynx.cfg` - se trouve un élément de configuration nommé
`EXTERNAL_MENU`. Cet élément permet d'exécuter des commandes arbitraires depuis
lynx en leur passant en argument le lien sous lequel se trouve le curseur. Dans
mon fichier de configuration j'ai les entrées suivantes :

	EXTERNAL_MENU:https\://www.youtube.com:360p:mpv --ytdl-format="18" --cache-secs=15 %s
	EXTERNAL_MENU:https\://www.youtube.com:720p:mpv --ytdl-format="22" --cache-secs=15 %s

Ainsi, quand le curseur de lynx se trouve sur un lien commençant par
`https://www.youtube.com`, appuyer sur `.` ouvrira un petit menu permettant de
lancer la vidéo en 360p ou 720p dans mpv. En combinant cela avec la fonction
suivante qui construit l'url de recherche de vidéo pour idiotbox

	yts() {
		lynx https://codemadness.org/idiotbox/\?q=$(echo "$@" | sed 's/ /%20/g')\&o=relevance
	}

je peux rapidement dans la console taper `yts machin bidule` pour avoir la
liste des vidéos de machin bidule, parcourir les résultats dans lynx, appuyer
sur `.` et choisir la résolution que je veux. Simple, rapide, efficace.

Si vous ne voulez pas dépendre de l'instance codemadness vous pouvez cloner le
[projet] et le compiler vous même. Cela génèrera un cgi pour une interface web
comme idiotbox mais aussi une interface en ligne de commande qui renvoie le
résultat de la recherche sous format texte.

	cli "Never Gonna Give You Up"
	Rick Astley - Never Gonna Give You Up (Official Music Video)
	URL:           https://www.youtube.com/embed/dQw4w9WgXcQ
	Atom feed:     https://www.youtube.com/feeds/videos.xml?channel_id=UCuAXFkgsw1L7xaCfnd5JJOw
	Channel title: Rick Astley
	Channelid:     UCuAXFkgsw1L7xaCfnd5JJOw
	Published:     12 years ago
	Viewcount:     1,298,048,819 views
	Duration:      3:33
	===
	Never Gonna Give You Up - Rick Astley (Lyrics) 🎵
	URL:           https://www.youtube.com/embed/34Ig3X59_qA
	Atom feed:     https://www.youtube.com/feeds/videos.xml?channel_id=UCvR2R7j218tzejtTsb_X6Rw
	...

Vous remarquerez au passage que la recherche renvoie, et ce peu importe
l'interface, le flux atom de la chaîne. Ca peut donc être un moyen comme un
autre de récupérer le flux rss d'une chaîne youtube.

## Limites

On s'affranchit pas de youtube

Streaming maison ?

Peertube ?

Questionner le besoin, moins de vidéos ?

Impact de rendre plus accessible et facile la conso de vidéo sur le temps
investit là dedans ? Anecdote perso, je regarde + de twitch depuis que j'ai un
bidule pour "m'abonner" sans compte. C'est agréable mais est-ce que c'était
vraiment l'effet recherché ?

Solutions :

* mutualiser les séances de visionage (projections/débats, ...)
* copier sur disque a vos amis
* préférer les podcasts (thinkerview, radio-france par exemple)
* préferer le peer2peer et les plateformes communautaires

[dark pattern]: https://fr.wikipedia.org/wiki/Dark_pattern
[Un bon article]: https://www.temesis.com/blog/youtube-com-un-modele-de-performance-web-environnementale/
[smartphone]: https://www.statista.com/statistics/1173543/youtube-viewing-time-share-device
[NewPipe]: https://newpipe.net/
[SkipTube]: https://github.com/Taregnaeem/SkipTube
[Termux]: https://termux.com/
[Ublock Origin]: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
[Enhancer for youtube]: https://addons.mozilla.org/en-US/firefox/addon/enhancer-for-youtube
[Sponsorblock]: https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/
[Youtube recommended videos]: https://addons.mozilla.org/en-US/firefox/addon/youtube-recommended-videos
[Youtube addon]: https://addons.mozilla.org/en-US/firefox/addon/youtube-addon
[RSS]: https://fr.wikipedia.org/wiki/RSS
[lecteurs RSS]: https://fr.wikipedia.org/wiki/Comparaison_des_agr%C3%A9gateurs_de_flux
[sfeed_web]: https://git.codemadness.org/sfeed/files.html
[mpv]: https://mpv.io/
[yt-dlp]: https://github.com/yt-dlp/yt-dlp
[HLS]: https://en.wikipedia.org/wiki/HTTP_Live_Streaming
[documentation]: https://github.com/yt-dlp/yt-dlp#format-selection
[ff2mpv]: https://addons.mozilla.org/en-US/firefox/addon/ff2mpv/
[le wiki du dépôt github]: https://github.com/woodruffw/ff2mpv/wiki
[idiotbox]: https://codemadness.org/idiotbox.html
[instance]: https://codemadness.org/idiotbox/
[sfeed]: http://codemadness.org/sfeed-simple-feed-parser.html
[README]: http://codemadness.org/git/sfeed/file/README.html
[projet]: https://git.codemadness/idiotbox
