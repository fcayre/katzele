#! page
%T Un outil pour la conjugaison
%A Arthur Pons
%D "[Insérer nom ici] est une interface sobre pour consulter les conjugaisons d'un verbe"
%P 2023-02-25
%S main

# [Insérer nom ici], une interface pour consulter les conjugaisons d'un verbe

> Cet outil ne dispose pas encore d'un nom. Si vous avez une idée je suis
> preneur.

Si vous êtes comme moi vous avez du mal avec les terminaisons des verbes en
français. Est-ce qu'il y a un s silencieux, où est l'accent circonflexe,
est-ce que ça prend un ou deux l ? Puisque je n'ai pas de Bescherelle sous
la main (peut-être devrais-je), je cherche "[verbe] conjugaison" dans un
moteur de recherche, je clique généralement sur le premier résultat, je
refuse les cookies, la newsletter, je cherche le temps et la personne
que je veux avec les yeux. C'est assez long et fastidieux, le site le
mieux référencé n'est pas toujours le même et je dois soit chercher celui
que je préfère dans la liste des résultats ou m'adapter à une interface
différente à chaque fois. Bref, c'est compliqué.

## Des interfaces alternatives et leurs usages

Quelqu'un du projet Bitreich avait pour projet d'interfacer tout un tas de site
web. L'idée est de pouvoir obtenir un service similaire voir équivalent mais à
travers des interfaces plus sobres, plus respectueuses de la vie privée , plus
performantes, plus accessibles. Un exemple est le site
[idiotbox](https://codemadness.org/idiotbox) qui interface la recherche youtube.

J'ai conscience qu'avec une interface pour alléger et simplifier le cas d'usage
décrit précédemment on ne s'attaque ni à un symbole du capitalisme de
surveillance ni à un grand émetteur de carbone. Cela dit l'implémentation est
relativement simple et m'est personnellement utile. Cela en fait, j'espère, un
bon exemple pour écrire un article.

### Interface web

TODO, installer le cgi sur le pc de marc, en faire un tit site

### Interface cli

### En local

Pour diverses raisons (ne pas dépendre de la disponibilité du service, parce
que vous utilisez plus les cli que les gui, que vous voulez l'interfacer avec
un éditeur de texte etc) il pourrait être souhaitable d'avoir le même usage via
une interface en ligne de commande. Pour cela vous pouvez télécharger les
sources qui permettent de faire fonctionner l'interface web

	git clone git://katzele.netlib.re/conjugaison-light

L'usage de l'outil est détaillé dans le README du projet.

Avec le script cgi nous pouvons, dans une interface en ligne de commande,
écrire :

	cgi [verbe] [mode] [temps] [personne]

et ne récupérer que l'information que l'on souhaite. Aucun clique inutile,
pas besoin de scanner un trop plein d'information. Par exemple :

	cgi prendre subjonctif présent je
	Subjonctif Présent que je prenne

Vous pouvez aussi faire tourner un serveur web sur votre pc et utiliser le
CGI.

## Fonctionnement

Cet outil dépend du site [conjugaisonfrancaise](https://conjugaisonfrancaise.com).
Il parse la page de ce site correspondant au verbe demandé pour stocker
le résultat sous une forme

	Prendre
	Indicatif
	Présent
	je pr{ends}
	tu pr{ends}
	il pr{end}
	...

Il tabule ensuite ces données pour en faire un TSV

	Indicatif	Présent	je pr{ends}
	Indicatif	Présent	tu pr{ends}
	Indicatif	Présent	il pr{end}
	...

Qui peut être facilement filtré si l'on précise les valeur des champs comme vu
précédemment avec les interfaces

	Subjonctif	Présent	que nous pr{enions}

Puis imprimé dans le console ou servi sous forme d'HTML. Dans la console il est
possible d'ajouter une étape pour colorier la terminaison. Chacune de ces
étapes est le résultat de l'ingestion de l'étape précédente par un nouveau
script. L'outil est donc assez modulaire si vous avez les sources. Vous pouvez
ne pas filtrer, conserver la version non tabulée ou faire votre propre script de
mise en page.

### Impact sur la consommation de données

Les pages du site que l'on a choisi sont relativement petites pour les standards
du web moderne. Elles vont de 150Ko à 200Ko. Le premier format de donnée que l'on
génère avec nos scripts pèse entre 1,6Ko et 2,5Ko. Nous sommes donc sur une diminution
de la quantité de donnée d'un facteur 100. Le format tabulé tourne autour du double ou
du quadruple entre 3,5Ko et 4Ko. Finalement, le format filtré peut descendre jusqu'à
quelques octets selon la finesse de la recherche. Si l'on veut servir les données
en HTML cela rajoutera environ 1Ko sur un retour non filtré. Pour la console
les séquences d'échappement pour colorier les terminaisons ajouteront 1Ko également.

Que ce soit la version web ou cli, il est possible d'enregistrer le premier
format de donnée pour ne plus avoir à requêter le site source si l'on a à
nouveau besoin du même verbe. La première requête fera donc transiter au plus
`200Ko + 5Ko = 205Ko` soit plus qu'une visite directe sur le site source.
Cependant toutes demandes successives du même verbe ne transféreront plus que
5Ko ou moins soit au plus 2,5% de la quantité de donnée initiale. Simplement en
retirant les données inutiles, en permettant le filtrage et avec un système de
cache rudimentaire. Si suffisamment de personnes utilisent le service en ligne,
l'économie de donnée peut s'avérer assez conséquente. A défaut ce rapide calcul
démontre qu'en repensant les interfaces, sans même forcément sortir du web,
nous pouvons obtenir le meilleur de plusieurs mondes.
