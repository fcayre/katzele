#! page
%T Kun, un outil de sondage
%A Arthur Pons, Pierre Noro
%D Génèse, fonctionnement et utilité de Kun, un outil pour faire des sondages notamment pour la prise de rendez-vous
%P 2022-12-21
%S main

# Kun

Du mot finlandais pour "Quand" comme dans "On se voit quand ?", Kun est un
outil permettant de faciliter la prise de rendez-vous à distance pour des coûts
humains, techniques et financiers faibles. Sur une idée et une implémentation
originale de Marc Chantreux. Merci à Pierre Noro pour la relecture.

## Enoncé du problème

Vous et une collègue de travail devez vous voir pour discuter d'un sujet
relativement urgent. Vous ne vous croiserez pas dans les jours qui viennent et
devez donc vous mettre d'accord sur une date à distance. Vous regardez vos
disponibilités dans votre agenda et écrivez le mail suivant :

> Salut,
> 
> Pour le point au sujet de la migration de machin je suis dispo demain
> après-midi sinon mardi 26 et jeudi 28 à partir de 15h.
> 
> A plus,
> Camille

Ce à quoi elle répond :

> Bonjour,

> C'est ok pour moi mardi mais plutôt 15h15, j'ai réunion de pôle jusqu'à 15
> et ça déborde souvent.
> 
> A mardi,
> Sasha

Puis :

> Aucun souci, à plus !
> 
> Camille

Cet échange est banal mais il contient de nombreuses étapes et informations.
Camille a communiqué son but. Elle a proposé trois dates sous plusieurs formats
différents. "Demain après-midi" est une date assez floue relative à la date
d'envoi du mail. "mardi 26 et jeudi 28 à partir de 15h." est plus précise, en
supposant que le contexte rende l'année et le mois évidents. Sasha a elle amendé
la demande et reçu une réponse favorable. Le rendez-vous est pris, ce sera
mardi 28 15h15.

On constate que malgré les ambiguités et la modification d'une des dates
proposées, l'échange s'est bien passé. Cela est probablement dû à deux choses.
Premièrement l'humain est incroyablement performant - du moins par rapport à
une machine - quand il s'agit gérer des données et situations ambiguës.  Cela
peut induire en erreur ou ralentir la prise de décision mais la présence de
deux formats de dates ambiguës et différents ne représente pas d'obstacle
majeur à la prise de rendez-vous. Le compromis sur l'heure de début de la
réunion non plus, il est raisonnable et justifié. Deuxièmement, le rendez-vous
ne se fait qu'entre deux personnes. Nous avons toutes et tous fait l'expérience
de l'explosion de la complexité de notre problème en fonction du nombre de
personnes impliquées. Non seulement la quantité de créneaux disponibles sera au
mieux équivalente et presque toujours plus faible, mais les soucis d'ambiguïté,
de compromis et d'éventuelles erreurs qui n'étaient que de simples détours dans
une conversation fluide deviennent des casses-têtes.

La solution à ce problème a été apportée par des outils webs de planification,
largement popularisée par [Doodle] et son alternative libre [Framadate] (en
quelque sorte lancée à l'Unistra [youpi] !). Ces outils prennent une approche
technique et, disons le, très dirigiste. Le problème de l'ambigüité est résolu
en proposant des interfaces web graphiques dans lesquelles l'utilisateurice
(utilisateur et utilisatrice en un mot) ne peut qu'insérer des données
correctes et non ambiguës. Les sondages ainsi créés doivent être stockés durant
une certaine durée afin que les destinatairices puissent y accéder et
éventuellement voir les réponses des autres. Pour corriger ses erreurs le
service doit pouvoir générer un état qui se souvient de chaque réponse et, a
minima, un lien pour y retourner quand ce n'est pas un système complet de
gestion de compte. Si le service possède un système de gestion de compte, une
partie du travail est déjà fait pour synchroniser les sondages avec des
calendriers existants et préremplir les réponses. Et ainsi de suite.

Bien que ces outils aient pu être simples par le passé, ils sont aujourd'hui
remplis de fonctionnalités complexes. Cela a un coût, Doodle emploie 3600
personnes (même si l'on imagine bien qu'elles ne sont pas toutes
informaticiennes). Framadate est paraît il difficile à maintenir, framasoft ne
souhaite plus le faire évoluer. Même si dans ce registre il y a très évidemment
de plus gros poissons auxquels s'attaquer on peut se poser la question de
l'impact environnemental de ces services qui, comme le reste du secteur du
numérique, nécessitent matériel et énergie.

De plus, leurs interfaces figées et graphiques génèrent de la dépendance et
nuisent à l'accessibilité. 

Pour trouver une date de réunion plus ou moins tout le monde saurait se mettre
d'accord sur les données à manipuler et les critères de décision à privilégier.
Du moins dans le monde du travail français je pense que ce sont des choses
assez consensuelles. Et pourtant vous êtes dépendants ou dépendante puisque ces
éléments sont choisis pour vous par des personnes qui vous sont éloignées
(physiquement, culturellement...) voir pas tout à fait humaines quand ce sont
des organisations. Ce n'est pas nécessairement une mauvaise chose, il est tout
à fait raisonnable d'accepter d'être dépendant des constructeurs d'IRM quand
cet outil peut vous sauver la vie en permettant aux médecins de prévenir un
cancer du cerveau. C'est moins raisonnable quand vos habitudes de travail ou
celles d'une association locale sont remises en questions parce que Doodle a
unilatéralement pris la décision de changer totalement d'interface graphique
(un éventuel cas d'étude sur ce sujet à venir) ou d'empêcher un ou une
utilisateurice de répondre pour un ou une autre qui n'aurait pas d'ordinateur
ou momentanément pas de réseau. Quid du temps perdu à se former, se déformer,
se reformer à chaque changement, à devoir jongler avec les abstractions
visuelles d'un service pour en appréhender de nouvelles sur un autre ? Qu'en
est-il du stress induit par le remplacement d'un service par un autre parce
que, face à la complexité rampante des fonctionnalités, la pression de se
conformer aux standards graphiques des GAFAMs et les moyens limités des
organisations notamment publiques, il est préférable de maintenir quelque chose
d'aussi bête qu'un logiciel de prise de rendez-vous à une échelle nationale
(voir Evento pour l'ESR) ?

Au sujet de l'accessibilité le sujet est sans appel. Les interfaces graphiques
de ces services font appel à nos représentations physiques du monde (cf le
calendrier dans Doodle). C'est un joli raccourci mais elles ne sauraient se
rendre palpable à, par exemple, une personne avec une déficience visuelle à
moins d'y aller à grand coup de CSS et de balise HTML spécialisées, rajoutant
ainsi une couche de complexité supplémentaire. On peut également noter que la
création et la réponse à ces sondages est assez lente et requiert de beaucoup
cliquer (ce qui peut également être une souci d'ergonomie chez certaines
personnes).

## Quelques principes pour Kun

Nous pourrions ici nous demander s'il n'y a pas d'autres chats à fouetter.
Certainement. C'est justement le rapport entre service rendu (assez anodin et
simple) et complexité (élevée) de cette catégorie d'application qui devrait
nous mettre la puce à l'oreille : on peut mieux faire.

Il ne s'agit pas d'essayer de faire un Doodle-like mais mieux. Trop souvent les
productions des milieux alternatifs numériques, notamment les libristes, se
sont faites en "alternative à", toujours en prenant un objet préexistant comme
référence sans réaliser que cela implique d'en hériter le cadrage des besoins,
la vision de ce à quoi l'informatique devrait nous servir. Pierre Noro
l'explique bien dans un article dédié à la question du [cloud souverain].

Il s'agit de reconnaître qu'aujourd'hui il n'existe pas de juste-milieu entre
la conversation textuelle déconstruite entre deux personnes et le logiciel
ultra complexe qui essaye de tout faire à lui tout seul. Pour qu'il soit
réellement plus utile que la première option mais reste durablement plus simple
que la seconde, il faut que notre système évite au moins d'avoir les même
défauts que les Doodle-like. A savoir :

	1. Qu'il ne se rende utile que là où l'informatique excelle, c'est-à-dire le
	   formalisme et l'expression de données quantitatives pour aider les
	   humains à partir d'une certaine échelle 

Je pense qu'il convient ici de mobiliser la citation de Peter
Salus au sujet de la philisophie Unix :

>"Write programs that do one thing and do it well. Write programs to work
>together. Write programs to handle text streams, because that is a universal
>interface."

Et la règle n°5 de Rob Pike à propos du développement en C :

>"Rule 5. Data dominates. If you’ve chosen the right data structures and
>organized things well, the algorithms will almost always be self-evident. Data
>structures, not algorithms, are central to programming."

Ce que ces citations nous enseignent c'est qu'il faut d'abord se préoccuper de
la donnée qui est manipulée. C'est en la formalisant que les instructions de
notre programme deviendront évidentes.

En réalité ce que l'utilisateurice fait quand il ou elle rédige son mail ou
construit son Doodle c'est faire une liste de dates. Si l'on reprend le premier
mail, en partant du principe qu'il est envoyé le jeudi 21 avril 2022 et que
l'après-midi commence à 14h, Camille a proposé la liste de dates suivante :

Le vendredi 22 avril 2022 à 14h, fuseau horaire de la France métropolitaine
Le mardi 26 avril 2022 à 15h, fuseau horaire de la France métropolitaine
Le jeudi 28 avril 2022 à 15h, fuseau horaire de la France métropolitaine

Nous avons déjà remarqué plus tôt qu'entre un petit nombre de personnes et dans
un contexte particulier il n'y avait pas besoin d'être aussi précis entre deux
humains - ce qui est, quand on y pense, la démonstration remarquable de
l'intelligence des humains par rapport aux machines. Cependant, écrit comme ça,
c'est aussi la démonstration que les opportunités de se tromper sont multiples
surtout si les invité·e·s sont nombreux ou nombreuses. Est-ce que tout le monde
vit dans le même fuseau horaire ? Est-ce que tout le monde parle la même langue
? Est-ce que tout le monde a l'habitude d'écrire ou de lire les dates sous ce
format ? La gestion du temps et sa représentation en informatique est un sujet
notoirement complexe, grand pouvouyeur de [memes].

Il en ressort que la meilleure chose que l'on puisse faire pour notre problème
est certainement de mettre tout le monde d'accord sur le format de date à
s'échanger. C'est, je pense, la réelle plus-value des Doodle-like. Il est
possible de se mettre oralement d'accord sur le format des dates, par exemple
[ISO_8601].

Pour minimiser les erreurs et se donner la possibilité d'automatiser certaines
tâches, il peut être intéressant de se reposer sur un outil informatique
existant. Afin de ne pas ajouter de complexité dans ce monde, d'être certain de
prendre tous les cas en compte et d'utiliser un outil léger pouvant fonctionner
sur presque tous les ordinateurs du monde, je propose de partir sur ce que
l'utilitaire `date` prend en [argument]. J'y reconnais un grand travers, le
logiciel peut imprimer des dates dans plusieurs formats différents en fonction
de LC_TIME de la machine mais n'accepte que de l'anglais en entrée. Ainsi :

	date -f-
	next monday +17 hours
	lun. 18 avril 2022 17:00:00 CEST
	lundi prochain +17 heures
	date: invalid date 'lundi prochain +17 heures'

Ce qui est assez cocasse puisque cela veut dire que si votre LC_TIME n'est pas
anglophone, `date` ne pourra pas lire ses propres sorties par défaut. Si l'on
est prêt·e à faire fi de ce souci et à utiliser `date`, c'est un monde de
possibilités qui s'ouvre à nous avec pour seul coût celui de s'être mis
d'accord sur notre façon d'écrire les dates.

Aussi, le comportement de `date` n'est pas constant à travers les UNIX. TODO retrouver l'article qui en parle.

	2. Qu'il puisse fonctionner sans "service", de façon totalement décentralisé

Très bien, nous voulons manipuler des dates qui soient lisibles et puissent
être produites par `date`. Et alors ? Comment utilise-t-on ça ensemble ?
Comment évite-t-on de recréer un service centralisé à la Doodle avec `date` qui
tourne dans le backend ?

Une personne veut organiser un rendez-vous avec 5 autres. Ces personnes se sont
préalablement mises d'accord pour s'envoyer des dates compatibles avec `date`.
La personne à l'initiative du rendez-vous écrit :

> Salut à toutes et à tous,
> 
> Voici les créneaux dispos de la 511 pour le groupe de travail "Un centre de
> calcul sans acheter de matériel neuf ?"
> 
> 2022-04-18T09:00+02:00
> 2022-04-18T10:00+02:00
> 2022-04-18T11:00+02:00
> 2022-04-19T09:00+02:00
> 2022-04-19T10:00+02:00
> 2022-04-19T11:00+02:00
> 2022-04-20T09:00+02:00
> 2022-04-20T10:00+02:00
> 2022-04-20T11:00+02:00
> 
> Bonne journée,
> Camille

Le format peut paraître opaque au premier regard mais est relativement simple.
Les créneaux sont les 18, 19 et 20 avril à 9h ou 10h ou 11h sur le fuseau
horaire +2h par rapport à l'UTC. Le message n'a plus aucune ambiguïté, est (à
peu près) lisible pour un humain et assimilable par `date`. Il est également
aussi accessible que le mail lui-même. Pour une personne en situation de
handicap, elle n'a pas à se soucier de rendre le site Doodle accessible en plus
de son client mail. Si le format est trop difficile à lire (ou à entendre
donc), il peut parfaitement être appréhendé par `date`. Par exemple, si votre
client mail est configuré de sorte à ouvrir les mails avec vim, vous pouvez
poser le curseur sur le bloc de date, le sélectionner avec "vip" appliquer un
filtre à la sélection avec "!" et taper la commande "date -f-" ce qui donne
chez moi :

	lun. 18 avril 2022 09:00:00 CEST
	lun. 18 avril 2022 10:00:00 CEST
	lun. 18 avril 2022 11:00:00 CEST
	mar. 19 avril 2022 09:00:00 CEST
	mar. 19 avril 2022 10:00:00 CEST
	mar. 19 avril 2022 11:00:00 CEST
	mer. 20 avril 2022 09:00:00 CEST
	mer. 20 avril 2022 10:00:00 CEST
	mer. 20 avril 2022 11:00:00 CEST

On peut formatter la date comme l'on veut, avec date -f- +'%A %d %B à %Hh%M'
par exemple :

	lundi 18 avril à 9h00
	lundi 18 avril à 10h00
	lundi 18 avril à 11h00
	mardi 19 avril à 9h00
	mardi 19 avril à 10h00
	mardi 19 avril à 11h00
	mercredi 20 avril à 9h00
	mercredi 20 avril à 10h00
	mercredi 20 avril à 11h00

Peu importe comment les destinatairices envisagent de lire ces dates, ils
peuvent répondre au sondage simplement en supprimant les lignes qui ne leur
conviennent pas. Il est par contre, nécessaire pour le traitement suivant de
renvoyer un format que `date` puisse lire. Un exemple de réponse pourrait être
:

> Salut Camille,
> 
> Je peux être là sur ces créneaux :
> 
> 2022-04-18T09:00+02:00
> 2022-04-18T10:00+02:00
> 2022-04-19T09:00+02:00
> 2022-04-20T09:00+02:00
> 2022-04-20T10:00+02:00
> 
> A plus,
> Sasha

Pour combiner format plus lisible et rédaction simple de la réponse on peut
appliquer un filtre zsh du type "cat > truc; paste truc =(date -f truc);rm
truc" TODO trouver une meilleure solution

	2022-04-18T09:00+02:00	lun. 18 avril 2022 09:00:00 CEST
	2022-04-18T10:00+02:00	lun. 18 avril 2022 10:00:00 CEST
	2022-04-18T11:00+02:00	lun. 18 avril 2022 11:00:00 CEST
	2022-04-19T09:00+02:00	mar. 19 avril 2022 09:00:00 CEST
	2022-04-19T10:00+02:00	mar. 19 avril 2022 10:00:00 CEST
	2022-04-19T11:00+02:00	mar. 19 avril 2022 11:00:00 CEST
	2022-04-20T09:00+02:00	mer. 20 avril 2022 09:00:00 CEST
	2022-04-20T10:00+02:00	mer. 20 avril 2022 10:00:00 CEST
	2022-04-20T11:00+02:00	mer. 20 avril 2022 11:00:00 CEST

On supprime les lignes que l'on ne veut pas puis on nettoie avec `cut -f1` :

	2022-04-18T09:00+02:00
	2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T11:00+02:00

Admettons que Camille reçoive les quatre autres réponses. Pour compiler les
résultats elle peut simplement les agréger dans un fichier texte en faisant,
par exemple, dans vim, "!cat >> hpc_sobre"). Ce fichier pourrait ressembler à
ceci :

	2022-04-18T09:00+02:00
	2022-04-18T10:00+02:00
	2022-04-18T11:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T11:00+02:00
	2022-04-18T10:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00
	2022-04-18T09:00+02:00
	2022-04-18T11:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T11:00+02:00
	2022-04-18T11:00+02:00
	2022-04-18T09:00+02:00
	2022-04-18T10:00+02:00
	2022-04-18T11:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T09:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T11:00+02:00

Nous visualisons bien ici le souci que représente le traitement humain de ces
données, même si elles étaient sous un format de date plus lisible. C'est
exactement dans ce genre de cas que l'informatique nous est utile, d'autant
plus que notre choix de format de date permet d'automatiser le traitement.

Par exemple, si l'on retient ce qui se fait souvent c'est à dire obtenir la
date à laquelle plus le gens sont disponibles il nous suffit de trier le
contenu et de compter les lignes identiques. D'abord un `sort`, qui renvoie :

	2022-04-18T09:00+02:00
	2022-04-18T09:00+02:00
	2022-04-18T09:00+02:00
	2022-04-18T10:00+02:00
	2022-04-18T10:00+02:00
	2022-04-18T10:00+02:00
	2022-04-18T11:00+02:00
	2022-04-18T11:00+02:00
	2022-04-18T11:00+02:00
	2022-04-18T11:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T09:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T10:00+02:00
	2022-04-19T11:00+02:00
	2022-04-19T11:00+02:00
	2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T09:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T10:00+02:00
	2022-04-20T11:00+02:00
	2022-04-20T11:00+02:00
	2022-04-20T11:00+02:00

Puis un `uniq -dc`

      3 	2022-04-18T09:00+02:00
      3 	2022-04-18T10:00+02:00
      4 	2022-04-18T11:00+02:00
      5 	2022-04-19T09:00+02:00
      3 	2022-04-19T10:00+02:00
      3 	2022-04-19T11:00+02:00
      4 	2022-04-20T09:00+02:00
      3 	2022-04-20T10:00+02:00
      3 	2022-04-20T11:00+02:00

Finalement un autre `sort` mais dans le sens décroissant, avec `-r`

      5 	2022-04-19T09:00+02:00
      4 	2022-04-20T09:00+02:00
      4 	2022-04-18T11:00+02:00
      3 	2022-04-20T11:00+02:00
      3 	2022-04-20T10:00+02:00
      3 	2022-04-19T11:00+02:00
      3 	2022-04-19T10:00+02:00
      3 	2022-04-18T10:00+02:00
      3 	2022-04-18T09:00+02:00

Tout cela peut évidemment se combiner, la commande magique serait alors

	sort | uniq -dc | sort -r | head

Je dis "magique" mais elle n'a pas grand chose de magique. Ces utilitaires sont
bien documentés et, je pense, beaucoup moins "magiques" que le monde du web et
ses interfaces. Même si je reconnais que leurs codes source ne sont pas des
plus [lisibles]. Certains groupes y travaillent comme [busybox].

On constate avec nos yeux que la date du 19 avril à 9h convient à tout le monde
sauf une personne. A nous de faire ce que l'on veut de cette information.

Ce qui est, à mon sens, remarquable dans ce scénario est qu'il met en lumière
la puissance d'un simple accord sur le format des données. Si un consensus est
trouvé sur un format de donnée qui soit textuel et facilement manipulable par
les outils Unix standards alors rien ou presque rien n'a besoin d'être
développé pour, dans un cas assez simple comme le nôtre, prendre un
rendez-vous. C'est ça le vrai "no code", un outil qui n'offre réellement pas de
code. Non pas du code supplémentaire offrant une abstraction tellement forte
que l'on croit pouvoir l'ignorer jusqu'à ce que l'on se rende compte que la
plateforme ne se conforme pas à nos attentes et qu'il faille soudainement
l'analyser sans l'avoir écrit.

La nature textuelle des données et la présence en local des outils nécessaires
à leur traitement permet de n'avoir recours qu'au mail comme moyen de
communication, sur des infrastructures déjà existantes et là où les habitudes
des utilisateurices ne sont généralement pas à construire.

Nous avons créé un "système" décentralisé, une technique que tout le monde peut
s'approprier, plutôt qu'un service dont nous serions dépendant·e·s. Nous pouvons
remercier l'ubiquité et la simplicité de la rédaction et la lecture des mails
pour cela. Ce ne sont heureusement pas encore devenues des activités jugées
trop compliquées pour mamie et papi à qui on aime donner un ipad parce que
c'est "convivial". 

Aparté à ce sujet
 
> Ma grand-mère sait un petit peu se servir de sa tablette, elle sait lire et
> envoyer des mails puisque ce sont souvent les deux première choses que l'on
> apprend. Elle peut donc utiliser Kun au moins en tant que participante au
> sondage. Peut-être qu'elle saurait utiliser Doodle mais si c'est le cas c'est
> parce que le temps qu'elle aura passé à se faire à la grammaire des designers
> de la Silicon Valley (Doodle est européen je sais) à travers l'utilisation
> d'iOS et de quelques sites internet modernes l'a familiarisé avec ces concepts
> et non pas parce que les designers sont des magiciens qui ont tout compris du
> fonctionnement du cerveau des personnes âgées. Je pense que le temps et
> l'énergie qu'il faut pour apprendre à lire et écrire un mail est inférieur au
> temps qu'il faut pour comprendre les interfaces web, même si elles tendent à
> toutes se ressembler. J'ai le sentiment que la majorité des personnes qui
> pensent que ce genre d'interfaces sont plus adaptées à des personnes éloignées
> du numérique n'ont
> 
> * pas passé suffisamment de temps avec les personnes éloignées du numérique
> * en sont tellement convaincues qu'elles n'enregistrent et ne mesurent pas les
>   cas contraires auxquels elles sont exposées
> * confondent leur propre adaptation à ces interfaces avec quelque chose
>   d'universel.
> 
> Je précise que je ne suis moi-même qu'assez peu au contact de ce genre de
> publics et que je ne connais rien à la science des interfaces, ce n'est pas
> mon domaine d'activité. J'imagine que j'aurais beaucoup à apprendre de la
> recherche menée sur ce sujet et que je suis au sommet de la courbe de
> Dunning-Kruger. Mon avis s'est construit en constatant plusieurs fois l'exact
> inverse de ce qui semble être pourtant considéré comme vrai par une majorité à
> savoir des personnes ayant beaucoup de mal à s'adapter aux interfaces
> graphiques modernes. Cela reste anecdotique. Au final, je sais pas, j'ai
> simplement l'intuition que c'est beaucoup plus compliqué que "iOS et jolis
> boutons et trucs qui bougent = tout le monde comprend instantanément et ligne
> de commande, texte et truc qui bougent pas = choses ésotériques impénétrables
> à moins d'être un hackerman".

Fin d'aparté

Attention, cela ne veux pas dire que notre système ne peut pas être transformé
en service centralisé.  Il est tout à fait possible de le faire et même assez
simple étant donné sa faible complexité. Il suffirait de générer quelques pages
webs avec un formulaire, recueillir les réponses sur un serveur, faire
automatiquement tourner la moulinette qui trouve la meilleure date et publier
une page avec le résultat. Si le sondage se tient entre des personnes ayant
toutes accès à un serveur distant on peut directement déposer sa réponse au
sondage via ssh, faire tourner la moulinette et consulter les résultats
soit-même. Peut-être est-il même possible de mettre le fichier dans un dépôt
git pour suivre les modifications et "garantir" la sécurité du sondage ?

Sa nature lui offre une affordance d'encouragement à l'utilisation d'outils
simples, manipulant du texte ligne par ligne. Autrement dit les outils Unix.
Ce n'est pas fait intentionnellement mais plutôt parce que les problèmes qui
méritent d'être en partie résolus par des outils numériques sont souvent de
nature à pouvoir être réduit en une collecte et un traitement linéaire de
données textuelles. Du moins quand on utilise beaucoup les outils Unix, c'est
comme cela que l'on est tenté de les aborder.
Ou alors cela fait parti de mon plan machiavélique pour inciter le maximum de
personnes à passer sous des distributions Unix et ainsi participer à résoudre
les soucis démocratiques et environnementaux liés au numérique. 😈

Côté performance la création et les réponses aux sondages vont dépendre de
l'infrastructure de mails si l'on opte pour ce canal. Ce n'est pas la chose la
plus fiable qui soit mais au moins on peut être certain que ça ne va pas
disparaître de si tôt et que l'on a pas à se soucier de l'hébergement (mais
une pensée à celles et ceux qui gèrent des serveurs mails donc). En utilisant
Kun, l'industrie entière du numérique travaille H24 à rendre votre service de
prise de rendez-vous opérationnel :)

Pour l'éventuel traitement automatique des réponses c'est hyper rapide. J'ai
dupliqué 1 000 000 fois le fichier de réponse précédent. Le résultat est un
fichier de 687MO contenant 31 310 000 lignes.  Voici le résultat sur un pc
équipé d'un intel core i5 7ème génération :

	time sort test_pref | uniq -dc | sort -r | head
	5050000 2022-04-19T09:00+02:00
	4040000 2022-04-20T09:00+02:00
	4040000 2022-04-18T11:00+02:00
	3030101 2022-04-20T11:00+02:00
	3030000 2022-04-20T10:00+02:00
	3030000 2022-04-19T11:00+02:00
	3030000 2022-04-19T10:00+02:00
	3030000 2022-04-18T10:00+02:00
	3029899 2022-04-18T09:00+02:00
	sort test_perf  148,60s user 1,49s system 327% cpu 45,853 total
	uniq -dc  1,92s user 0,19s system 4% cpu 45,852 total
	sort -r  0,00s user 0,00s system 0% cpu 45,850 total
	head  0,00s user 0,00s system 0% cpu 45,849 total

Forcément on s'imagine mal à quoi ça pourrait servir mais c'est simplement pour
pouvoir dire, telle une bonne startup, "ça scale" et "tkt on passe à l'échelle
crème, notre tech elle est deep". Bah passez mieux à l'échelle que
l'infrastrucure mondiale des mails et un traitement de 680 000 réponses par
seconde sur mon ordi bof. La comparaison est évidemment bancale, ça ne
comprends pas l'écriture du fichier etc.

### Discussions et limites

A partir de là nous pourrions faire le large inventaire de ce que notre système
ne permet pas de faire facilement :

	* C'est long et chiant de taper les dates sous quelque format que ce soit

Marc Chantreux a développé un petit outil en AWK qui permet d'écrire des dates
sous la forme

	april 18 21; d+ 1 2; w* 3

Ce qui va générer la date du 18 avril à 21h de cette année plus celle des deux
jours qui suivent même heure, ces trois jours répétés sur les trois semaines
qui viennent. Cela donne :

	<<. respl
	s april 18 21; d+ 1 2; w* 3
	.
	april 18 21+0weeks+0days+0hours
	april 18 21+0weeks+1days+0hours
	april 18 21+0weeks+2days+0hours
	april 18 21+1weeks+0days+0hours
	april 18 21+1weeks+1days+0hours
	april 18 21+1weeks+2days+0hours
	april 18 21+2weeks+0days+0hours
	april 18 21+2weeks+1days+0hours
	april 18 21+2weeks+2days+0hours

J'ai étendu le script pour qu'il puisse également gérer les décalages d'heures.
C'était très simple malgré mon inexpérience avec awk puisque le script ne gère
aucune logique de temps, c'est simplement une réécriture de la ou les lignes
qu'on lui donne en entrée. C'est possible grâce à la syntaxe relative de date
qui permet d'ajouter ou soustraire des unités de temps (années, jours, secondes
etc) à une date donnée avec les opérateurs - et +.
Le code se trouve [ici](./respl).

	* Il n'y a pas de moyen évident de corriger une erreur de date, d'amender sa
  	  réponse si jamais notre calendrier change

Si Camille m'envoie un premier mail avec les dates

	2022-04-18T21:00+02:00
	2022-04-20T21:00+02:00
	2022-04-26T21:00+02:00
	2022-04-27T21:00+02:00
	2022-05-02T21:00+02:00
	2022-05-04T21:00+02:00

Puis le lendemain un autre en disant

	Désolé en fait je vais pas pouvoir être là le 26, ceci dit me suis libéré
	le 25
	
	2022-04-18T21:00+02:00
	2022-04-20T21:00+02:00
	2022-04-25T21:00+02:00
	2022-04-27T21:00+02:00
	2022-05-02T21:00+02:00
	2022-05-04T21:00+02:00

Je peux toujours chercher le premier bloc de date consécutives dans le fichier
de réponse et le remplacer par celui-ci. C'est évidemment plus pratique si le
second mail est threadé avec le premier mais c'est une bonne pratique de toute
façon. 

TODO donner une tite commande.

Ce n'est pas parfaitement fiable, peut-être envisager la possibilité d'ajouter
un identifiant de la personne après les dates ? Ou utiliser git ?

* Cela gère des dates précises et non pas des intervales de temps

En fait si. Si l'on a les réponses suivantes :

	2022-04-18T11:00+02:00 2022-04-18T12:00+02:00
	2022-04-18T09:00+02:00 2022-04-18T10:00+02:00
	2022-04-19T10:00+02:00 2022-04-19T11:00+02:00
	2022-04-20T09:00+02:00 2022-04-20T10:00+02:00
	2022-04-19T11:00+02:00 2022-04-19T12:00+02:00
	2022-04-20T10:00+02:00 2022-04-20T11:00+02:00
	2022-04-19T09:00+02:00 2022-04-19T10:00+02:00
	2022-04-20T11:00+02:00 2022-04-20T12:00+02:00
	2022-04-18T10:00+02:00 2022-04-18T11:00+02:00

`sort` et `uniq` fonctionnent parfaitement bien dessus (je crois, tester les
créneaux qui peuvent être à l'intérieur d'autres créaneaux TODO). Si l'on veut
utiliser `date` pour modifier le format, en le rendant plus lisible par exemple
on peut filtrer le texte précédent :

	sed 's/ /\n/' | date -f- +"%A %d %B à %Hh%M" | paste - -
	lundi 18 avril à 11h00	lundi 18 avril à 12h00
	lundi 18 avril à 09h00	lundi 18 avril à 10h00
	mardi 19 avril à 10h00	mardi 19 avril à 11h00
	mercredi 20 avril à 09h00	mercredi 20 avril à 10h00
	mardi 19 avril à 11h00	mardi 19 avril à 12h00
	mercredi 20 avril à 10h00	mercredi 20 avril à 11h00
	mardi 19 avril à 09h00	mardi 19 avril à 10h00
	mercredi 20 avril à 11h00	mercredi 20 avril à 12h00
	lundi 18 avril à 10h00	lundi 18 avril à 11h00

Qu'on peut rendre encore plus lisible avec :

	sed 's:\t.*à: -:' | column -nts 'à'	
	lundi 18 avril      11h00 - 12h00
	lundi 18 avril      09h00 - 10h00
	mardi 19 avril      10h00 - 11h00
	mercredi 20 avril   09h00 - 10h00
	mardi 19 avril      11h00 - 12h00
	mercredi 20 avril   10h00 - 11h00
	mardi 19 avril      09h00 - 10h00
	mercredi 20 avril   11h00 - 12h00
	lundi 18 avril      10h00 - 11h00

Bref, on peut imaginer ce que l'on veut, si vous avez un format préféré vous
pouvez le créer, en faire un alias et tout exécuter en une fraction de seconde.
Il faut simplement se souvenir d'envoyer un format assimilable par `date`.

	* On ne peut pas voir les réponses des autres participant·e·s à moins que
	  tout le monde fasse "reply all" au premier mail

Nous pouvons nous demander si cela est toujours préférable. Il est généralement
possible de désactiver ou activer cette fonctionnalité dans les Doodle-like.

Dans Doodle, si les résultats du sondages sont ouverts cela veut dire que la
réponse d'une personne est rendu visible à tout le monde. Sinon, seule la
personne à l'origine du sondage a accès aux réponses (et Doodle aussi n'est-ce
pas). Dans notre système les réponses sont dans un mail. Si l'on veut rendre
notre réponse accessible à tout le monde, il suffit donc de choisir de
"répondre à tous". Sinon on utilise "répondre à". Nous avons recréé la
fonctionnalité pour un coût nul.

Cela à l'avantage supplémentaire de renverser le choix du périmètre de
diffusion de ses données. Habituellement c'est l'initiateur du sondage qui
choisit de les rendre publiques ou pas, ici les répondants et répondantes
restent propriétaires des données qu'ils ou elles génèrent.

	* On ne sait pas qui a répondu quoi et donc on ne peut pas différencier le
  	  "poids" donné à chaque réponse

A la charge du ou de la créateurice du sondage d'ajouter avant ou après chaque
date un identifiant par répondant (que ce soit le nom ou un numéro). Il est
ensuite possible d'omettre ce champ lors du traitement.

TODO tester

	* Il n'y a pas de mécanisme d'exclusion de créneau en fonction de son
  	  calendrier existant

Ce type de fonctionnalité est, je pense, à proscrire dans l'élaboration d'un
système comme celui-ci. Ce système a un but donné, vouloir fortement
l'interfacer avec d'autres systèmes, comme ceux de gestion de calendrier par
exemple, ferait prendre le risque d'une sur-spécialisation et d'une complexité
non nécessaire. Le risque est d'autant plus grand que les systèmes avec
lesquels nous voudrions nous interfacer sont eux très complexes.

Cela dit la simple stupidité de notre système permet en théorie de l'interfacer
avec n'importe quoi. Si une personne ou un groupe de personnes veut le faire,
c'est possible. Il ne faut simplement pas que Kun devienne "Un système d'aide à
la prise de rendez-vous et aussi qui sait s'authentifier sur un service en
ligne et parser du format bidule pour y lire un calendrier et detecter les
conflits".

 	* Il n'y a pas ou peu de garantis de l'intégrité du vote

Effectivement, il sera difficile voir impossible de garantir que personne n'ait
voté deux fois, que la créateurice du sondage n'ait pas modifié les résultats
etc. Nous pourrions investiguer les solutions techniques et/ou
organisationnelle pour s'approcher d'une garantie. Ce serait certainement très
intéressant. Alternativement nous pouvons estimer que 90% (99% ?) des
sondages/votes ne nécessitent pas de telles garanties et qu'il est donc inutile
de complexifier l'outil pour répondre à un besoin inexistant. Des besoins de
votes numériques garantis intègres existent et nécessitent des outils
spécifiques.

Quoi qu'il en soit pour éclaircir l'adéquation de Kun avec les pratiques
habituelles de sondages il faudrait mener des expérimentations en condition
réelles à une certaine échelle. Nous espérons pouvoir le faire prochainement.

[Doodle]: https://fr.wikipedia.org/wiki/Doodle.com
[Framadate]: TODO
[youpi]: TODO
[cloud souverain]: https://acteurspublics.fr/articles/pierre-noro-le-cloud-est-avant-tout-un-sujet-politique
[memes]: https://i.redd.it/tqxwb35d54n81.jpg
[7]: https://i.redd.it/uhboopf6f8u71.jpg
[argument]: https://www.gnu.org/software/coreutils/manual/html_node/Date-input-formats.html
[ISO_8601]: https://en.wikipedia.org/wiki/ISO_8601
[lisibles]: https://git.savannah.gnu.org/gitweb/?p=coreutils.git;a=blob;f=src/sort.c
[busybox]: https://git.busybox.net/busybox/tree/coreutils/sort.c?id=cc7d2e21780c28608b00a4faf0fed297527bcbf4
