#! page
%T Guide pour contribuer au collectif
%A Arthur Pons
%D Un document pour aider les nouveaux et nouvelles membres à s\'intégrer au collectif
%P 2023-01-03
%S main

# Introduction

Si vous lisez ce document c'est que vous êtes intéressé·e par le fait de
participer au collectif Katzele. Ou qu'une personne du collectif a été un peu
trop enthousiaste en pensant que c'était le cas. Ça arrive.

Ce document vous guidera dans la configuration de certains outils, l'adoption
de certaines pratiques et vers les activités que vous pouvez mener dans le
collectif. Vous n'avez pas besoin de tout lire. A vrai dire vous n'avez
*besoin* de rien lire.

## Activités à mener dans le collectif

Il n'y a heureusement pas de tâche administrative récurrente donc pas de
"poste" à pourvoir type scrétariat, trésorerie etc. Le plus important est de
venir avec un intérêt pour l'informatique, la sobriété, la conversation
(beaucoup de conversation) et le partage (très niais dit comme ça).

Il est aujourd'hui très utile pour le collectif d'avoir :

  * Des perspectives nouvelles, notamment non techniques, sur l'histoire de
    l'informatique, ce qui a déterminé la forme qu'elle prend aujourd'hui et
    comment cela pourrait être modifié à l'avenir.
  * Des contacts pour organiser des conférences et tables rondes sur les sujets
    que nous abordons ainsi que pour y inviter des personnes pertinentes.
  * Des volontés pour documenter nos échanges et idées, entre autre en
    contribuant à ce site.
  * Des personnes utilisant, en conditions si possible réelles, les outils
    produits afin d'avoir des retours et de les améliorer.
  * Des lecteurs et lectrices ainsi que des auditeurs et auditrices critiques
    pour nous aider à améliorer nos discours, sur le fond et sur la forme.
  * De la veille technique et sociale sur ce qu'il se passe dans le petit monde
    de la sobriété numérique, des lowtechs, des amishs et autre écoterroristes.
  * Une technique miracle pour que Marc devienne un processus FIFO et non pas
    AIRO (any in, random out).
  * Des ami·e·s pour nous dire qu'on fait des trucs cools.

Si vous vous sentez capable d'accomplir un ou plusieurs éléments de cette liste
sentez vous libre de le faire, ça nous ferait grand plaisir ! Il n'y aucune
définition nette de qui est dans le collectif ou pas. N'ayez pas l'impression
que vous n'êtes pas légitime parce que vous n'avez qu'envoyé deux trois mails
ou corrigé quelques fautes dans un article. A l'inverse on ne criera pas sur
tous les toits que vous êtes dorénavant un châton alsacien parce que vous nous
avez poliment écoutez blablater à un salon y'a 3 mois.

## Avoir accès aux dépôts git et y contribuer

### Accès en lecture

Pour avoir accès en lecture et anonymement aux dépôts faites :

	git clone git://katzele.netlib.re/nom_du_depot

La liste des dépôts est sur la [page d'accueil](http://katzele.netlib.re).

### Contribuer

Pour contribuer aux dépôts il vous faut générer une paire de clef ssh ed25519
et envoyer la clef *publique* à la liste de diffusion du collectif.  Vous
pouvez la trouver sur la page d'accueil.

Sur la plupart des distributions linux vous pouvez générer la clef en utilisant la commande

	ssh-keygen -t ed25519 -C "commentaire"

Le commentaire peut vous permettre de différencier facilement les clefs entre
elles si vous avez plusieurs paires. Vous devriez avoir deux nouveaux fichiers
dans le dossier `~/.ssh`, `id_ed25519` et `id_ed25519.pub`. Ces deux fichiers
forment une paire. Celui sans extension est la clef privée, il ne faut
**jamais** la communiquer à qui que ce soit. Celui avec l'extension `.pub` est
la clef publique, vous pouvez la communiquer à qui vous voulez, cela ne
représente *aucun* risque. C'est ce dernier fichier que vous devez transmettre
à la liste. Vous pouvez simplement copier coller son contenu dans
un mail ou le mettre en pièce-jointe, il n'y a pas de procédure particulière,
c'est simplement du texte.

Une fois votre clef publique installée sur le serveur du collectif (à savoir,
pour le moment, le fixe perso d'un de nos membres), vous serez en capacité
d'accéder aux dépôts git via ssh et d'intéragir avec `kit`, notre petit outil
de type "forge".

Aujourd'hui kit permet de créer des dépôts et de lister les dépôts existants.
Commençons donc par là pour connaître la liste des projets que l'on pourra
cloner.\
Créons un nouveau "host" ssh. Pour cela vérifions si le fichier `~/.ssh/config`
existe. S'il n'existe pas il faut le créer. Ajoutons les lignes suivantes :

	host kit
	hostname katzele.netlib.re
	port 1359
	requestTTY no
	passwordAuthentication no
	user git

Nous devons maintenant créer un lien symbolique portant le même nom de host et
pointant vers la commande `ssh-argv0` :

	sudo ln -s $(which ssh_argv0) /chemin/vers/le/dossier/dans/lequel/vous/voulez/mettre/lexecutable

Il est préférable que le chemin en question soit dans votre variable
d'environnement `PATH`. Il pourrait par exemple être `/usr/local/bin`.

Si vous avez correctement suivi les étapes précédentes et que votre système ne
diffère pas trop du mien vous devriez dorénavant pouvoir lancer la commande
kit.

	create a new repo
	
	        mk repo_name short description
	
	list existing repositories
	
	        ls
	
	
	examples of valid commands if your bit alias is configured
	
	        git remote add kit kit:yourstuff
	        git push -u origin main
	        git clone kit:yourstuff

En faisant un `kit ls` vous verriez quelque chose comme :

	convivo tools of digital conviviality
	katzele.netlib.re        le site du collectif sans nom
	mkcrapdeb       faire des paquets debian facilement
	ososcape        jeu pour ososphere 2022
	ezfold
	downloadmusic   télécharger de la musique
	hltv-light      Récupérer des infos d'hltv dans la console
	twitch-online   Vérifier si des chaînes twitch sont en direct
	fip     Ecouter fip dans la console
	cinema-light    Récupérer le programme des cinémas star
	conjugaison-light       Vérifier la conjugaison d'un verbe
	wordreference-light     traduire des mots
	chantreux.ai    Fitness functions for the AI called Chantreux
	playlist-arthur Que des bangers
	image-optim     Optimiser et réduire des images

Admettons que nous souhaitons travailler sur le contenu du site, il nous suffit
de faire `git clone kit:nomdudepot` à savoir :

	git clone kit:katzele.netlib.re

Si tout cela vous semble être magique je vais écrire un article pour expliquer
le fonctionnement de ce système.

TODO Ecrire un article expliquant kit, demander à Marc de tenir un dépôt à jour coordonnier mal chaussé


## Discuter avec les membres par mail

La plupart des échanges à distance se font par mail. Il existe une liste de
diffusion sur laquelle n'importe qui peut lancer n'importe quel sujet.

katzele@framalistes.org

Vous pouvez évidemment utiliser n'importe quel client de messagerie et rédiger
vos mails comme vous le voulez. Un certain nombre de personnes du collectif ont
cependant quelques préférences, à vous de voir si vous souhaitez les adopter.

1. Ils préfèrent la rédaction et la lecture de mails en texte pur plutôt qu'en
HTML.

Si vous ne le savez pas sachez que les mails peuvent prendre (au moins ?) deux
formes, du texte pur et de l'HTML. Par défaut beaucoup de clients de mails (les
webmails type gmail.com, certains clients lourds) vous font rédiger vos mails
en HTML.\
Cela apporte certaines fonctionnalités comme celles de pouvoir mettre en gras,
ajouter des images ou de la couleur. En contrepartie cela alourdi les mails,
introduit des vulnérabilités, permet de vous pister et contraint les clients
mails à intégrer de la complexité pour comprendre et afficher de l'HTML.\
A vous de peser le pour et le contre. Si vous souhaitez mettre en forme un mail
en texte brut vous pouvez envisager une syntaxe en markdown ou quelque chose de
similaire. Si vous savez que les personnes à qui vous écrivez n'ont pas de
défience visuel vous pouvez vous inspirer de l'ascii art et faire de belles
choses.

2. Ils n'utilisent pas de top posting

Quand vous répondez à un mail, beaucoup de clients vont copier le mail auquel
vous répondez en bas de votre réponse. Cela alourdit votre mail et, dans une
mesure dont on peut débattre, ne fait pas vraiment sens à la lecture. Nous nous
attendons à lire le texte le plus vieux en premier.\
Il est possible, avec un client configuré pour, d'automatiquement "citer" le
mail auquel on répond dans sa réponse. La syntaxe la plus courante est celle
d'ajouter "> " en début de ligne et de répondre au mail dans le corps. Par
exemple

Mail 1

	Salut,
	
	blablabla
	tuctructruc
	
	A plus

Réponse

	> Salut,
	Salut !
	
	> blablabla
	Oui bien sûr, je suis d'accord
	
	> tuctructruc
	Ah ça non !
	
	A toute

Cela facilite la lecture, un peu de la même façon que les applications de
messagerie instantanées modernes permettent d'écrire un message "en réponse à"
un autre message. Evidemment cela peut couper des lignes, c'est une bonne idée
de les réécrire si nécessaire.

Si ce sujet vous intéresse vous pouvez lire ce [court article] de Drew Devault.
Il y fait un parallèle entre le courrier traditionnel et le mail. Les courriers
que vous recevez de vos ami·e·s ou de votre famille seraient les mails en texte
brut sans (trop) de top posting. Les publicités ou divers courriers vaguement
utiles de votre banque / votre assurance etc seraient les mails HTML hyper mis
en forme, très impersonnels. Vous lisez avec enthousiasme les premiers, vous
jetez ou redoutez la lecture des seconds. La forme du courrier est un
relativement bon indicateur de l'importance que le contenu aura pour vous.
Ironiquement, bien que l'HTML permette plus de mise en page, mon expérience est
qu'il m'est plus facile de déchiffrer un mail en texte brut qui n'aura ni
image, ni signature élaborée, nécessairement qu'une seule police etc. Cela est
attribuable aux abus d'utilisation d'HTML plutôt qu'à la supériorité du texte
brut mais la question de la contre-productivité de l'inclusion d'HTML dans les
mails se pose alors. Cela-dit lisez quand même les courriers de votre banque.

Un ingénieur allemand dont je peine à retrouver le site avait formulé quelque
chose d'assez similaire pour le web. Il semble y avoir une corrélation non
négligeable entre l'apparence "datée" d'un site et la quantité/densité de
données pertinentes dessus. D'un côté nous aurions des sites tel que celui de
[vim] ou le [blog de Drew Devault] voir même Wikipedia. Le premier dans un
style souvent moqué (pas qu'à tort notamment pour des soucis d'accessibilité)
mais efficace, le second dans un style plus "moderne" faisant presque
entièrement fi du CSS. De l'autre nous aurions les millions de sites quasi
identiques, présentant le dernier framework js à la mode, la dernière crypto
etc. Pour en trouver il suffit d'aller voir n'importe quel coin de la liste
[ici]. Ce sont généralement des sites dont l'apparence change souvent pour
suivre les modes et souffrant d'une densité voir même d'une quantité
d'information excessivement faible. Si vous êtes déjà allé·e sur un site, que
vous avez scrollé la page d'accueil, vu les petits pictos "fusée" pour dire
"rapide" et "groupe de bonhomme" pour dire "teamwork yeah" et que vous vous
êtes dit·e "je sais toujours pas ce qu'iels font" vous comprenez de quoi je
veux parler. D'ailleurs si l'on comprend le terme information comme il est
définit dans la [théorie de l'information] cela fait sens. Pour faire simple,
une donnée contient autant d'information que de "surprise", de données qui
viennent modifier notre connaissance du monde telle qu'elle était avant de la
recevoir. Ainsi, si votre site non seulement contient peu de données mais
qu'en plus il ressemble à tous les autres sites - et donc à l'image attendue
d'un site en 2023 - il sera difficile de le différencier d'autres sites et d'en
extraire de la connaissance.

[court article]: https://drewdevault.com/2016/04/11/Please-use-text-plain-for-emails.html
[vim]: https://www.vim.org/
[blog de Drew Devault]: https://drewdevault.com/
[ici]: https://coinmarketcap.com/
[théorie de l'information]: https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27information
