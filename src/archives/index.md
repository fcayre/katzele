#! page
%T Archives des interventions du collectif
%A Arthur Pons
%D Liste complète des interventions du collectif Katzele
%S main

# Archives des interventions du collectif Katzele

La liste des interventions du collectif. Quelques évènements manquent à
l'appel, ils apparaitront ici au prix d'un peu d'archéologie.

%

grep '^%T\|^%L\|^%A\|^%D\|^%R' src/events/2022 |
	tail -n+2 |
	paste - - - - - |
	awk -F'\t' -v now="%D "$(date -I'minutes') '{if($4<now){print $0}}' |
	awk 'BEGIN {FS="	";OFS="	"} { cmd = "date -d " substr($4,4) " +\"%%D %A %d %B %Y\""
		   while ( ( cmd | getline fmtDate) > 0 ) { 
			   $4 = fmtDate
		   }
		   close(cmd);
	}1' |
	sed -Ee 's/\\//g'\
	     -e 's/"//g'\
	     -e 's/%T (.+)	%L (.+)	%A (.+)	%D (.+)	%R (.+)/\4  \n**\1** - \5  \n\2 - \n*\3*\n/' |
	save_md main
