#! page
%T Une exploration collective du convivialisme numérique
%A katzele
%D Nous sommes un collectif de personnes interrogeant la place et l\'avenir du numérique dans une société contrainte de prendre en compte les limites planétaires
%S main

# Qui sommes-nous?

Katzele est un collectif ouvert (cf. [contact](contact.html)) de citoyens et
citoyennes impliquées dans la promotion de la
[convivialité](https://fr.wikipedia.org/wiki/Outil_convivial), de la culture
unix pour un large public et de l'avènement d'un numérique plus durable.

## Rapport avec le libre

Nous nous reconnaissons dans les valeurs du logiciel libre et reconnaissons les
4 libertés comme fondamentales mais nous nous éloignons de tous les outils qui
perpétuent un rapport de dépendance entre les producteurs et productrice (et/ou
hébergeurs et hébergeuses) et les usagers et usagères. Nous estimons que les valeurs
fondamentales que doivent porter le numérique sont
l'émancipation/l'autonomisation, la responsabilisation et l'entre-aide.

## Influences des outils sur l'humain et inversement 

La notion d'inter-determination entre l'outil et la société nous semble
centrale dans la résistance au consumérisme et ses conséquences : le saccage de
nos cultures, de nos sociétés et des conditions de survie du vivant.

Il est communément admis dans le collectif que les outils et la technique ne
sont pas neutres. C'est en reconnaissant que l'outil détermine aussi les
rapports entre humains que l'on se pose les bonnes questions sur la forme
qu'ils devraient prendre et l'usage que l'on devrait en faire. Cette attention
portée aux outils, leurs histoires, leurs capacités, leurs affordances peut
être confondue avec une forme de zèle religieux ou de fétichisation de la
technique. C'est pourtant bien parce que nous souhaitons ne pas dépolitiser la
technique et ignorer le pouvoir qu'elle a que nous nous attardons autant sur
leurs caractéristiques et avons des avis "passionnés" en décalage avec la
trompeuse prise de distance nécessaire pour stipuler que les outils ne sont
"que des outils" et que la seule variable digne d'intérêt est celle des choix
d'usage.

## D'où parlons nous ?

Le collectif part du constat suivant, posé par la communauté scientifique : les
crises socio-environnementales menacent les conditions d'habitabilité de la
terre pour les humains et les non humains. Afin d'éviter certains impacts et
s'adapter à ceux auxquels nous faisons déjà face, nous devons dès à présent
circonscrire nos activités aux limites planétaires.

Parmi ces activités, le numérique. Nous devons y imposer un régime de sobriété
au même titre que le secteur énergétique, le secteur des transports etc. Cela
implique d'utiliser moins de numérique, de le retirer là où il n'est pas
souhaitable, pas utile.\
Le collectif admet un second constat, celui qu'il existe, et continuera
d'exister pour un certain temps, des sous-ensembles de pratiques permises par
le numérique qu'il serait préférable de maintenir même au delà de la
circonscription de nos vies aux limites planétaires. Il convient de définir ces
sous-ensembles communément selon les groupes de personnes, de leurs besoins et
de leurs environnements. Le collectif cherche à outiller les personnes pour
qu'elles puissent rendre aussi durable que possible ces pratiques à un coût
humainement et environnementalement modeste.

Pour répondre à ce problème le collectif mobilise aujourd'hui deux cultures ou
courants philosophiques : [la
convivialité](https://fr.wikipedia.org/wiki/Outil_convivial) et la [philosophie
Unix](https://fr.wikipedia.org/wiki/Philosophie_d%27Unix).  Nous trouvons dans
la convivialité un cadre fécond pour la critique d'un numérique trop complexe
et trop privateur ne permettant pas aux humains de le mobiliser pour
s'émanciper, y compris dans leurs quêtes de sobriété. Nous trouvons dans la
philosophie Unix une culture ingénieuriale informatique à la fois répandue et
propice à l'implémentation de systèmes informatiques davantage conviviaux et
sobres.

C'est donc dans l'exploration de l'intersection entre :

  * une lecture grave et lucide de l'urgence environnementale
  * la technocritique du numérique moderne et de ses effets inhibants sur la
    créativité et les capacités d'adaptation des humains
  * l'héritage des pratiques et normes des principaux architectes du système
    Unix

que le collectif se place pour faire émerger un nouvel imaginaire d'un
numérique moins invasif, plus utile, plus sobre et plus juste.

## Points d'éventuels dissensus ou d'approfondissement

Par souci de transparence et pour susciter les débats nous posons ci dessous
les sujets sur lesquels nos avis divergent, sur lesquels nos motivations sont
différentes ou sur lesquels se place le front de nos idées.

Bien que les deux soit intrinsèquement liés - d'où l'utilisation du terme
"socio-environnementaux" - les sensibilités aux problèmes sociaux d'une part et
aux problèmes environnementaux d'une autre varient d'un membre du collectif à
un autre.

L'idée qu'il existera toujours des sous-ensembles de pratiques numériques
souhaitables peut faire débat. Le collectif connaît l'existence de collectifs
opposés au numérique dans son ensemble et lui prêtant des caractéristiques
intrinsèques l'empêchant d'être éthique. Voir les Décâblés ou Technologos.\
Les membres du collectifs sont plus ou moins sensibles à ces idées et
articulent leurs positions dans Katzele en fonction. Il semblerait que sans
pour autant souhaiter la disparition du numérique Katzele ait possiblement plus
d'affinités avec ces collectifs qu'avec d'autres groupes alternumériques ne
posant pas l'impératif de sobriété et ne souhaitant pas s'aventurer vers des
rapports hommes/machines significativement différents de ceux qui sont
aujourd'hui majoritaires.

La référence à la convivialité est assez peu stable.\
Premièrement par manque de connaissance et de culture en philosophie des
techniques. La majorité des membres de Katzele sont à la fois très
enthousiastes à l'idée d'aborder ces sujets et relativement incultes en la
matière. C'est ce qui nous motive à dialoguer et, si elles le veulent bien,
intégrer des personnes ayant des compétences en philosophie, sociologie,
histoire, science du design etc.\
Deuxièmement parce que nous n'avons pas la réponse à la question pourtant
évidente : le numérique peut-il être convivial ? Si la réponse est non alors
nous nous adonnons soit à la perpétuation d'un système technique
intrinsèquement inapte à être utile pour résoudre les problèmes
socio-environnementaux soit à un processus de "convivialisation", par analogie
avec "low technicisation" de Stéphane Crozat, qui serait tout de même
souhaitable car donnant naissance à un numérique "moins pire".\
Dernièrement parce qu'il n'est pas encore clair si le collectif souhaite
mobiliser le concept de "convivialité" d'Illich ou le courant philosophique du
"Convivialisme" inspirée par Ivan Illich, ayant fait l'objet de deux manifestes
et dont le sociologue Alain Caillé est la figure de proue. Les deux ne
s'opposent pas, le premier est la caractéristique d'un outil - et à fortiori
d'une société en fonction des outils qu'elle emploie - à rendre plus efficace
les personnes sans dégrader leurs autonomies. Le second un projet de société
plus large souhaitant faire advenir une nouvelle façon de vivre ensemble au
delà du modèle néo-libérale aujourd'hui majoritaire. Il faut faire attention
aux termes employés, par exemple dans le titre précédent. Sommes-nous à
l'intersection "écologisme/unix/convivialité" ou
"écologisme/unix/convivialisme" ?

Le choix de la culture Unix comme principal cadre technique pour
l'implémentation d'outils numériques conviviaux et la critique de ceux qui ne
le sont pas est quelque peu par défaut et ne fait pas consensus. Elle a
l'avantage d'être déjà bien répandue dans les milieux libristes et rapidement
mobilisable pour produire des démonstration technique. Cela dit ses
implémentations aujourd'hui les plus courantes (les grandes distributions linux
et les outils qui leur sont couramment associés) ne sont pas exemptes de
critiques et possiblement pas les plus fécondes ni les plus créatives. Les
communautés qui font vivre ces outils ne sont pas toujours très réceptives aux
enjeux politiques qui devraient nous pousser à rester en mouvement et être
perpétuellement critique du contenu de notre boite à outil.\
Une partie du collectif est intéressé par l'exploration de systèmes plus
moins répandus tel que [Plan
9](https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs) ou
[Oberon](https://en.wikipedia.org/wiki/Oberon_(operating_system)) qui
pourraient se révéler être culturellement et/ou techniquement plus
intéressants à explorer que les distributions Linux.
