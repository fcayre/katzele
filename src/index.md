#! page
%T Une exploration collective du convivialisme numérique
%A katzele
%D Nous sommes un collectif de personnes interrogeant la place et l\'avenir du numérique dans une société contrainte de prendre en compte les limites planétaires
%S main

# Katzele

Si [notre démarche](about.html) vous questionne, vous inspire ou vous donne
envie de vous engager, n'hésitez pas à [échanger avec notre
collectif](contact.html). 

## Activités

Nous contribuons à des travaux de recherche, des expérimentations techniques,
participons à des tables rondes et aux efforts pédagogiques sous toutes les
formes.

Nous développons, hébergeons et utilisons des versions conviviales (pour le
moment expérimentales) des outils suivants tels que gitlab, le générateur de
site statique hugo, framadate/doodle etc. La liste complète est disponible sur
notre serveur git.

Ces alternatives sont parfois des outils à part entière, parfois des systèmes
de nature protocolaire pour répondre au besoin sans développement ou encore
l'agencement d'un ensemble d'outils conviviaux déjà existants. Elles recouvrent
très rarement l'ensemble des fonctionnalités des outils originaux. Cela est
voulu, nous ne souhaitons pas recréer des outils à  l'identique mais identifier
les besoins essentiels auxquels ils répondent pour les réimplémenter plus
succinctement.

Nous utilisons

* mumble+ssh+tmux pour les visioconférences
* IRC pour le clavardage
* la messagerie électronique comme moyen de communication décentralisé
  et résilient aux avaries du réseau.

## Articles

Attention, c'est rarement abouti.

%

find src/articles/ -type f -name '*.md' |
	xargs grep -Hm3 '^%T\|^%A\|^%P' |
	paste - - - |
	awk -F'\t' -v now="%P "$(date -I) '{if(substr($3,length($3)-12,13)<=now){print $0}}' |
	sed -Ee 's,src/(.*).md:%T (.*)	src/.*.md:%A (.*)	src/.*%P (.*),* \4 - [\2](\1.html) - \3,'\
	     -e 's,([0-9]{4})-([0-9]{2})-([0-9]{2}),\1/\2/\3,' |
	sort -rn |
	save_md main

%S main

## Notes diverses

Attention, c'est brut de chez brut.

%

find src/notes/ -type f -name '*.md' |
	xargs grep -Hm3 '^%T\|^%A\|^%P' |
	paste - - - |
	awk -F'\t' -v now="%P "$(date -I) '{if(substr($3,length($3)-12,13)<=now){print $0}}' |
	sed -Ee 's,src/(.*).md:%T (.*)	src/.*.md:%A (.*)	src/.*%P (.*),* \4 - [\2](\1.html) - \3,'\
	     -e 's,([0-9]{4})-([0-9]{2})-([0-9]{2}),\1/\2/\3,'\
	     -e 's:\\::g' |
	sort -rn |
	save_md main

# %S main
# 
# ## Nos interventions à venir - [flux rss](./events.rss)
# 
# %

grep '^%T\|^%L\|^%A\|^%D\|^%R' src/events/2022 |
	tail -n+2 |
	paste - - - - - |
	awk -F'\t' -v now="%D "$(date -I'minutes') '{if($4>now){print $0}}' |
	tac |
	awk 'BEGIN {FS="	";OFS="	"} { cmd = "date -d " substr($4,4) " +\"%%D %A %d %B %Y\""
		   while ( ( cmd | getline fmtDate) > 0 ) { 
			   $4 = fmtDate
		   }
		   close(cmd);
	}1' |
	sed -Ee 's/\\//g'\
	     -e 's/"//g'\
	     -e 's/%T (.+)	%L (.+)	%A (.+)	%D (.+)	%R (.+)/\4  \n**\1** - \5  \n\2 - \n*\3*\n/' |
	save_md main

%S main

## Nos interventions passées

Nos cinq dernières interventions.

Parfois menées de A à Z, parfois intégrées à un évènement plus grand. Merci à
tous les collectifs et toutes les personnes nous ayant invité !

[Archives exhaustives](./archives/index.html)

%

grep '^%T\|^%L\|^%A\|^%D\|^%R' src/events/2022 |
	tail -n+2 |
	paste - - - - - |
	awk -F'\t' -v now="%D "$(date -I'minutes') '{if($4<now){print $0}}' |
	awk 'BEGIN {FS="	";OFS="	"} { cmd = "date -d " substr($4,4) " +\"%%D %A %d %B %Y\""
		   while ( ( cmd | getline fmtDate) > 0 ) { 
			   $4 = fmtDate
		   }
		   close(cmd);
	}1' |
	sed -Ee 's/\\//g'\
	     -e 's/"//g'\
	     -e 's/%T (.+)	%L (.+)	%A (.+)	%D (.+)	%R (.+)/\4  \n**\1** - \5  \n\2 - \n*\3*\n/' |
	head -n$(dc -e '5 6 * p') |
	save_md main

%S main

## Le code produit

Ci dessous les dépôts des logiciels que nous produisons. Vous pouvez les
télécharger en faisant

	git clone git://katzele.netlib.re/nom_du_depot

Si vous voulez contribuer lisez [ce tuto](http://katzele.netlib.re/articles/accueil/).

%

curl -s http://katzele.netlib.re/git-ls.txt |
sed -r '
    1iNom du dépôt | Description du dépot | Lien de clone\
    :--|---|---
    /DEPRECATED/d
    /\t/!d
    s,(.*)	(.*),\1 | \2 | <git://katzele.netlib.re/\1>,
' | save_md main

%S main

