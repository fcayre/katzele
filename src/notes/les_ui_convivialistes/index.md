#! page

%T Notes sur les UI convivialistes
%A mc
%P 2022-09-17
%S main

Cette note est très inspirée par ma
[pratique actuelle de vim](https://github.com/eiro/talk-acme-changed-my-life),
elle-même inspirée par ma découverte de l'interface de
[9front](http://9front.org/) en 2008.

# retour vers le futur

Les premiers écrits d'Illich sont contemporains des premières versions d'Unix
dont la philosophie est magnifiquement décrite dans les premiers chapitres
de "The Art Of Unix Programming". La logithèque moderne qui s'inspire de ces
principes (disponible sur les BSD et les forks de plan9 et dans une version
de moins en moins représentative sous linux). J'y vois les restes d'une culture
de l'ingénierie et de l'économie qui s'éteint avec la prise de conscience de
l'immense rentabilité d'un productivisme instauré pour un effort de guerre et
au mépris de toute autre considération. Illich en prophétisait la disparition
pendant que les ingénieurs d'ATT en produisait une des dernières grandes
incarnations. L'"effort de paix" est une composante non négligeable de la dérive
consumériste.

Si cette culture n'a été valorisé par aucune de nos élites (politiques,
économiques et académiques) pendant les 70 derniers, nombre d'ingénieurs et de
techniciens se sont impliqué à titre personnel dans la transmission et
l'évolution de cette sous-culture de la technologie (culture des hackers).
Leurs écrits et logiciels sont la meilleure illustration disponible de
la recherche de compromis entre efficience, maintenabilité et fonctionnalité
que constitue le convivialisme numérique.

L'informatique personnelle semble avoir introduit ou généralisé trois problèmes
majeurs de la culture numérique actuelle:
* la culture du logiciel privateur (qui rend impossible une attitude autre que le consumérisme)
* la monoculture du WIMP (par transmission d'idée entre Xerox et le duo apple/microsoft?)
  qui semble avoir totalement invisibilisé les évolutions des interfaces nées de la rencontre
  entre une longue expérience des outils textuels et la possibilité naissante d'afficher des
  interfaces graphiques riches.

Le web actuel est la rencontre entre la culture de l'informatique personnelle
et l'opportunité de réduire le cout de tout ce qui n'a pas de valeur commerciale
en déformant a l'excès ce qui devait être un système de documentation distribué
et en privant l'utilisateur de la simple liberté d'afficher et manipuler
les données comme il le souhaite, constitue (avec les suites bureautiques WYSIWYG)
le pire exemple de la pire induction de culture technologique par des impératifs financiers.

Toutefois je ne suis pas omniscient: il existe d'autres futurs qui ont
été rasés par l'informatique personnelle qu'il nous faut explorer et
je ne connais que trop peu les technos relatives a l'embarqué (IoT,...).

# à propos de vim

Si je conviens que vim est déjà trop (et inutilement?) gros, il est la
meilleure base à ma connaissance pour illustrer mon propos puisqu'il
possède des fonctionnalités qui se rapprochent fonctionnellement de
`plumber(4)` et respecte la philosophie unix en permettant un recours
aux pipes et fichiers pour être étendu: vim peut lire depuis,
écrire depuis et filtrer avec un pipe. Il est ainsi possible de
fournir les fonctions présentes dans les logiciels plus gros en
s'appuyant sur des programmes existants.

Exemple 1: statistiques sur la taille du texte en cours d'édition

	:%w !wc

Exemple 2: l'envoyer par mail

	:%!mutt -s 'exemple de message' mc@example.com

Exemple 3: un navigateur de fichiers (combiné avec gf)

	:0r !ls

Il existe des usages plus évolués qui restent relativement
abordables. Voici comment écrire une extension permettant
d'interroger interactivement `whatis(1)` pour sélectionner
un manuel quand la touche K est pressée:

Coté vim:

	command -nargs=* Whatis
	\ silent exec "!sf_whatis <args> > ~/.v"
	\ | new
	\ | setf man
	\ | 0r ~/.v
	ru ftplugin/man.vim
	nmap K :Whatis <cword><CR>

Coté shell:

	sf_whatis() {
		whatis "$1"      |
		fzf              |
		tr -s '( )' '\n' |
		sed 2q           |
		tac              |
		xargs -r man
	}

# du bienfondé des TUI

La possibilité de pouvoir piloter visuellement une boucle
d'interaction entre des commandes et du texte est, il me semble,
le principal principe des
[TUI](https://en.wikipedia.org/wiki/Text-based_user_interface#Oberon)
(Text based user interface) dont la pratique était assez courante
dans les années 80. Ca n'est pas étonnant: c'est l'époque à laquelle
s'est mélangé une longe expérience des pratiques d'outils textuels
(et la compréhension intuitive de ces avantages) et la généralisation
des interfaces graphiques.

La productivité des interfaces textuelles est due à mes yeux à:
* la concision des commandes
* le principe (documenté qqpart?) de "muscle memory" (comme apprendre à
  faire du vélo et ne plus y penser quand les interfaces issues de la philosophie
  PARC/Apple sont certes plus intuitives mais nécessitent une implication permanente
  de l'utilisateur)
* la possibilité d'automatiser simplement

# Idées de pistes à suivre

Jean a cité "Sabre" (peut-être [sabre](https://www.sabre.com/)?)
et [Dassault Systèmes](https://www.3ds.com/) (vus sur des salons?)
comme exemple d'éditeurs de ce genre d'outils. Il y a peut-être
des réflexions de ce coté.

Plan9 semble avoir été inspiré par
[Oberon](https://en.wikipedia.org/wiki/Oberon_%28operating_system%29),
développé à l'[ETH de Zurich](https://en.wikipedia.org/wiki/ETH_Zurich)
et qui semble être l'objet d'activités récentes (2013 et 2020). Ces personnes
sont peut-être à contacter.

Dans un autre domaine: [blender](https://www.blender.org/) est autre logiciel
qui m'a interpellé pour la productivité rendue possible par des séquences ou la
souris ne sert qu'à ajuster des actions déclenchées par le clavier.
Nous pourrions peut-être profiter de retours originaux d'utilisateurs avancés
(Christophe Scherrer, ancien collaborateur de la DUN, est un bon interlocuteur).

# que faire du web ?

En plus du coté "continum d'usages", j'aimerais aussi creuser l'idée
d'assumer que le web:

* ne pourra jamais être une platteforme convivialiste
* concentre hui une grande partie des usages

et donc assumer de remettre du js/wasm pour introduire des possibilités
d'interaction proches des TUI (par exemple: filtrer ou trier avec de
petits DSL).

Possible experimentation: rendre
[respl](https://git.unistra.fr/mchantreux/shelpers/-/blob/main/repsl/respl)
disponible via websocket dans un premier temps?

# autres liens

* [The Server Room Show: None of these are Unix](https://blog.tsr-podcast.com/index.php/2021/05/13/episode-76-oberon-plan-9-inferno/)
* la section "how does it work" de l'article [The technology that changed air travel](https://retool.com/blog/air-travel-software/)

# anectodes illustrant notre propos

qui est en gros:
* les utilisateurs savent et veulent apprendre
* les UIs actuelles ne valent pas les anciennes en terme de productivité
  (l'idée était de mettre en avant la "découvrabilité" pour sacrifier les metiers de support).

* il y a encore 10 ans de ça, nous passions pour des sans coeur quand
  nous proposions une syntaxe wiki (type markdown, textile,...) à un utilisateur
  standard.
* Remy (git à la cop 2 étudiante) non seulement la cop s'est bien passée mais
  les étudiants se disent fiers d'avoir pu apprendre à utiliser un outils qui
  réduisent considérablement notre dépendence à la concentration et aux besoins
  infrastructurels.
* lorsque j'étais magazinier, la séquence des accélérateurs était utilisée pour
  nommer les taches à réaliser. par exemple: "fais un bcu" voulais dire
  "remplis le formulaire accessible par la combinaison d'accélerateur bcu"
  (bon/commande/urgent dans le menu)
