#! page

%T Chronologie du code du site de Katzele
%A Arthur Pons
%P 2023-01-13
%S main

# Une chronologie de l'évolution du code de Francium tel qu'utilisé par Katzele

Je me suis demandé comment avait évolué la base de code du site, notamment sa
taille mise au regard des fonctionnalités ajoutées. Pour cela je suis allé
chercher la première ligne du message de chaque commit de ce dépôt ainsi que la
quantité de ligne de code ajoutée au dépôt. Ce qui compte comme du code est
soumis à interprétation. J'ai décidé de supprimer les commentaires par exemple.
J'inclus des choses telles que le template HTML et le CSS. Je devrais
probablement les exclure si je voulais m'intéresser spécifiquement à Francium
et non pas à ce site. Je n'inclus pas le code qui génère du markdown dans les
pages. Ces bouts de code peuvent être complexes, assez nombreux et doivent être
maintenus mais ne font pas vraiment partie de Francium. Pour info aujourd'hui
(13 janvier 2023) cela représente quatre lignes.

Évolution du total nombre de lignes (sans commentaire, sans ligne vide, avec template
heredoc) par commit :

	  500 +--------------------------------------------------------------------+
	      |                                                                    |
	  450 |-                                                                  -|
	      |                                                                    |
	  400 |-                                                      AAAAAAAA    -|
	      |                                                                    |
	  350 |-                                                                  -|
	      |                                                                    |
	  300 |-                                                                  -|
	      |                                                   AAAA             |
	  250 |-                 A                           AAAAA                -|
	      |                   AAAAAAAAAAAAAAAAAAAAAAAAAAA                      |
	  200 |-                A                                                 -|
	      |     AAAAAAAAAAAA                                                   |
	  150 |-                                                                  -|
	      |                                                                    |
	  100 |- AAA                                                              -|
	      |                                                                    |
	   50 |-                                                                  -|
	      +--------------------------------------------------------------------+

Le premier saut représente l'ajout d'une css et d'un moteur pour en générer.
Le second l'ajout d'un système pour générer un flux atom, le troisième un autre
système de génération de flux, la chute qui suit est due à la supression du
premier.  Ensuite quelques modification du template html font bouger les choses
et le dernier saut est l'ajout du css actuellement en production.

Le détail en différence de nombre de ligne par commit :

> +107 - first release\
>    0 - ajout du lien "None of these are Unix"\
>   +1 - liste des notes depuis la page principale\
>    0 - ajout d'une todo list\
>    0 - ajout d'un lien dans notes ui\
>    0 - liens vers bitreich et suckless\
>    0 - correction proposée par tjay\
>    0 - lien vers ecoinfo\
>  +63 - introduction des styles\
>    0 - annecdotes sur l'ui\
>    0 - retour vers le futur et autres ajouts\
>    0 - ajout d'une section contact\
>    0 - usage direct de save_md\
>   -1 - template layout: stylesheet optionnel avec $STYLE\
>    0 - Correction de fautes, proposition de légères modifications du texte introductif\
>    0 - création d'une section "nos activités"\
>   +1 - *make* it work\
>    0 - Ajout d'une note concernant les interfaces alternatives à youtube.com Dépôt d'un patch permettant de modifier sfeed_curses\
>    0 - Ajout du nom de ou des auteur·ices des notes sur la page d'accueil Est-ce que c'est utile ? Attention ! Casse si la note ne renseigne pas son auteurice\
>    0 - Retrait de l'option --safe pour cmark La version de cmark packagée pour debian n'a pas cette option mais c'est bien le comportement par défaut Son ajout explicite ne fait dont qu'introduire un bug\
>   +3 - Modification du makefile pour permettre d'ajouter des fichiers annexes aux notes\
>    0 - Ajout d'un patch pour pouvoir choisir l'ouverture audio/video dans sfeed_curses Modification en conséquence de l'article\
>   +3 - Modif du makefile pour générer les raw et commenter\
>    0 - Modification de la page d'accueil\
>    0 - Ajout des évènements passés et futurs à la page d'accueil\
>    0 - Ajout des dates aux notes\
>  +16 - Ajout d'un script pour générer un flux rss/atom/sfeed correspondant aux notes\
>  +37 - ajout de bin/atomic: RFC\
>  -17 - Suppression de rss au profit d'atomic\
>    0 - Relecture, ménage\
>    0 - Modification de la TODO après discussions à la roue libres\
>    0 - Modification d'Atomic, modification des markdown en fonction\
>    0 - Ajout d'un dossier flux et du fichier pour 2022\
>   +3 - Modification de page et du script de la page d'accueil pour les notes\
>    0 - Modification du makefile pour copier les sources md\
>   +3 - Modification d'atomic et du fichier des évènements\
>   +1 - Génération automatique des évènements passés et futurs dans la page d'accueil\
>    0 - Documentation de la syntaxe des fichiers dans events\
>   +2 - Modification d'atomic pour utiliser les lieux et les ressources\
>   +1 - Flux rss pour les interventions, obstacle à la génération du flux des articles\
>    0 - Ajout des dates de tous les évènements passés\
>   +2 - Ajout d'un champ fin d'intervention et affichage dans le flux rss\
>    0 - Oups, oublié de renommer certains champs %D en %P\
>    0 - Ajout d'un README\
>    0 - Tri chronologique des évènements à venir (je crois)\
>    0 - Progrès sur la documentation\
>    0 - Modification du makefile\
>    0 - La documentation avance\
>    0 - Progrès sur la partie page du README\
>    0 - Toujours plus de documentation dans le README\
>    0 - Ajout des futurs évènements aux techdays 13\
>    0 - Tri par ordre chronologique des évènements à venir\
>    0 - Modification du patch sfeed_curses pour passer un arg à sfeed_plumber\
>    0 - Ménage README\
>    0 - Modification et ajout de ressources pour évènement eveille\
>    0 - Transformer les dates des évènements à venir en format lisible par un humain\
>    0 - Modification de l'article youtube\
>    0 - ajout d'une note todo sur les conférences passées\
>    0 - Ajout d'une meta donnée visible ou non L'ajout se fait avec un alias %V, l'absence %V indique que la note est en cours de rédaction Utilisation de deux grep pour filtrer les notes dans src/index.md @Note un filtre pour exclure %V serait surement une solution plus adaptée.\
>    0 - Ajout d'une note sur des idées d'outils convivialiste pour le web Mise à jour de la note sur le design pour encourager l'ajout de commentaires et retours Respect de la structure avec notes/date/titre/index.md pour toutes les notes\
>    0 - ajout d'un exemple de réseau internet alternatif\
>    0 - Affichage dates des évènement passés et ajout des slides pour prez techdays 13\
>    0 - Ajout de la vidéo de la conf avec Adrastia\
>    0 - Modifications diverses page d'accueil\
>    0 - On retire les articles déposés aux mauvais endroits\
>    0 - Modif de la fonct de visibilité et modification des articles en fonction\
>    0 - Intégration des nouveaux articles de Derek et de la gestion de la visibilité\
>    0 - Ajout de références dans l'article premier pas hors du web\
>  +23 - Intégration de certaines des propositions de Derek pour le template html\
>    0 - Ajout d'un article sur la gestion de playlists\
>    0 - Progression sur l'article à propos des playlists\
>    0 - Publication de l'article sur les playlists\
>    0 - Modif du script des notes pour publier à la date annoncée et pas le lendemain\
>    0 - Modification de la structure de la page d'accueil\
>    0 - édition de la note sur le web\
>    0 - On crédite comme il se doit qui de droit :)\
>   +3 - Modif d'atomic et du fichier des interventions pour obtenir un flux atom valide\
>    0 - Ajout article Kun, sép "articles"/"notes" modif page d'accueil, archives events\
>    0 - Début de prise de notes sur la convivialité\
>    0 - Ajout d'un guide pour nouvelleaux\
>    0 - Ajouts et modifications diverses du contenu\
> +146 - proposition d'un premier style proche des TUI, les couleurs sont provisoires\
>    0 - mise à jour des critères pour le design du site\
>   -3 - intégration du CSS de Derek\
>    0 - header > head\
>    0 - style.css comme dépendance de page\
>    0 - virer 1 niveau d'arbo\
>    0 - Ajout du lien de la vidéo pour une intervention aux techdays\
>    0 - Ajout du transcript d'une conversation avec chatgpt\
>    0 - Progrès sur la documentation de Francium\
>    0 - Progression sur la doc de Francium\
>    0 - Suppression du doc test laissé par Arthur\
>    0 - Ajout d'une nouvelle note sur la chrono du code du site\

Quelques remarques me viennent à l'esprit en voyant ce résultat :

Les commits qui ajoutent du code sont rares, 23 contre 72

Les ajouts sont généralement petits. Quand ils ne le sont pas le commit porte
sur le css du site ou l'ajout d'une grosse fonctionnalité (générer un flux
atom)

Quand il y a des retraits ils sont très petits. D'ailleurs certaines parties du
site ne sont actuellement (13 janv. 2023) pas utilisées mais n'ont pour autant
pas été supprimées. Soit par oubli soit parce qu'elles pourront l'être à
nouveau à l'avenir. Ces fichiers représentent 132 lignes à l'écriture de cet
article, 53 pour shylus, un système de génération de css actuellement abandonné
au profit du css écrit à la main de Derek. 79 pour pug, un système de
templating html actuellement abandonné au profit un template écrit à la main.
Autant shylus ne serait peut-être jamais utilisé, autant pug devrait revenir
sur le devant de la scène.

Une bonne partie non négligeable du code hors css est du templating (36
lignes).

Si l'on retire le code de ces deux systèmes le nombre de ligne de Francium
passe à 262. Moins 147 de CSS on tombe à 115, moins les 36 de templating on
tombe à 79. En somme il y a actuellement bien moins de 100 lignes de shell à
proprement parler qui fait réellement fonctionner Francium. Cela pose la
question de la nécessité du reste. Peut-être un ménage à venir.

Les ajouts de fonctionnalités me semblent générer des quantités raisonnables de
code. Par exemple la copie de fichier "annexes" accompagnant les md de src vers
root et la copie des sources de src vers root n'a ajouté que six lignes dans le
makefile. Aujourd'hui cela permet d'héberger au côté de l'article youtube les
fichiers de patch d'sfeed_curses et d'avoir accès aux sources en modifiant le
`.html` en `.md` dans l'url. Autre exemple le commit avec le message "Modif de
la fonct de visibilité et modification des articles en fonction" intègre une
gestion du conditionnement de la visibilité d'un lien dans une page
"gratuitement" sur la base d'un alias qui existait déjà et en ajoutant un peu
de code dans la page où l'on souhaite afficher des liens et pas d'autre.

Dans l'ensemble, si l'on considère l'évolution de Francium depuis septembre
2022 (date du premier commit) à aujourd'hui je pense que l'on peut dire que la
croissance de la base de code est maîtrisée. Avant tout parce qu'en quelques
mois la quantité de fonctionnalités supplémentaires n'est pas très élevée
(autant par manque de temps que par volonté de ne pas en ajouter), mais
également parce que la structure de cet outil permet de le faire à un coût
raisonnable.

Les données du total des lignes de code par commit :

> 107 - first release\
> 107 - ajout du lien "None of these are Unix"\
> 108 - liste des notes depuis la page principale\
> 108 - ajout d'une todo list\
> 108 - ajout d'un lien dans notes ui\
> 108 - liens vers bitreich et suckless\
> 108 - correction proposée par tjay\
> 108 - lien vers ecoinfo\
> 171 - introduction des styles\
> 171 - annecdotes sur l'ui\
> 171 - retour vers le futur et autres ajouts\
> 171 - ajout d'une section contact\
> 171 - usage direct de save_md\
> 170 - template layout: stylesheet optionnel avec $STYLE\
> 170 - Correction de fautes, proposition de légères modifications du texte introductif\
> 170 - création d'une section "nos activités"\
> 171 - *make* it work\
> 171 - Ajout d'une note concernant les interfaces alternatives à youtube.com Dépôt d'un patch permettant de modifier sfeed_curses\
> 171 - Ajout du nom de ou des auteur·ices des notes sur la page d'accueil Est-ce que c'est utile ? Attention ! Casse si la note ne renseigne pas son auteurice\
> 171 - Retrait de l'option --safe pour cmark La version de cmark packagée pour debian n'a pas cette option mais c'est bien le comportement par défaut Son ajout explicite ne fait dont qu'introduire un bug\
> 174 - Modification du makefile pour permettre d'ajouter des fichiers annexes aux notes\
> 174 - Ajout d'un patch pour pouvoir choisir l'ouverture audio/video dans sfeed_curses Modification en conséquence de l'article\
> 177 - Modif du makefile pour générer les raw et commenter\
> 177 - Modification de la page d'accueil\
> 177 - Ajout des évènements passés et futurs à la page d'accueil\
> 177 - Ajout des dates aux notes\
> 193 - Ajout d'un script pour générer un flux rss/atom/sfeed correspondant aux notes\
> 230 - ajout de bin/atomic: RFC\
> 213 - Suppression de rss au profit d'atomic\
> 213 - Relecture, ménage\
> 213 - Modification de la TODO après discussions à la roue libres\
> 213 - Modification d'Atomic, modification des markdown en fonction\
> 213 - Ajout d'un dossier flux et du fichier pour 2022\
> 216 - Modification de page et du script de la page d'accueil pour les notes\
> 216 - Modification du makefile pour copier les sources md\
> 219 - Modification d'atomic et du fichier des évènements\
> 220 - Génération automatique des évènements passés et futurs dans la page d'accueil\
> 220 - Documentation de la syntaxe des fichiers dans events\
> 222 - Modification d'atomic pour utiliser les lieux et les ressources\
> 223 - Flux rss pour les interventions, obstacle à la génération du flux des articles\
> 223 - Ajout des dates de tous les évènements passés\
> 225 - Ajout d'un champ fin d'intervention et affichage dans le flux rss\
> 225 - Oups, oublié de renommer certains champs %D en %P\
> 225 - Ajout d'un README\
> 225 - Tri chronologique des évènements à venir (je crois)\
> 225 - Progrès sur la documentation\
> 225 - Modification du makefile\
> 225 - La documentation avance\
> 225 - Progrès sur la partie page du README\
> 225 - Toujours plus de documentation dans le README\
> 225 - Ajout des futurs évènements aux techdays 13\
> 225 - Tri par ordre chronologique des évènements à venir\
> 225 - Modification du patch sfeed_curses pour passer un arg à sfeed_plumber\
> 225 - Ménage README\
> 225 - Modification et ajout de ressources pour évènement eveille\
> 225 - Transformer les dates des évènements à venir en format lisible par un humain\
> 225 - Modification de l'article youtube\
> 225 - ajout d'une note todo sur les conférences passées\
> 225 - Ajout d'une meta donnée visible ou non L'ajout se fait avec un alias %V, l'absence %V indique que la note est en cours de rédaction Utilisation de deux grep pour filtrer les notes dans src/index.md @Note un filtre pour exclure %V serait surement une solution plus adaptée.\
> 225 - Ajout d'une note sur des idées d'outils convivialiste pour le web Mise à jour de la note sur le design pour encourager l'ajout de commentaires et retours Respect de la structure avec notes/date/titre/index.md pour toutes les notes\
> 225 - ajout d'un exemple de réseau internet alternatif\
> 225 - Affichage dates des évènement passés et ajout des slides pour prez techdays 13\
> 225 - Ajout de la vidéo de la conf avec Adrastia\
> 225 - Modifications diverses page d'accueil\
> 225 - On retire les articles déposés aux mauvais endroits\
> 225 - Modif de la fonct de visibilité et modification des articles en fonction\
> 225 - Intégration des nouveaux articles de Derek et de la gestion de la visibilité\
> 225 - Ajout de références dans l'article premier pas hors du web\
> 248 - Intégration de certaines des propositions de Derek pour le template html\
> 248 - Ajout d'un article sur la gestion de playlists\
> 248 - Progression sur l'article à propos des playlists\
> 248 - Publication de l'article sur les playlists\
> 248 - Modif du script des notes pour publier à la date annoncée et pas le lendemain\
> 248 - Modification de la structure de la page d'accueil\
> 248 - édition de la note sur le web\
> 248 - On crédite comme il se doit qui de droit :)\
> 251 - Modif d'atomic et du fichier des interventions pour obtenir un flux atom valide\
> 251 - Ajout article Kun, sép "articles"/"notes" modif page d'accueil, archives events\
> 251 - Début de prise de notes sur la convivialité\
> 251 - Ajout d'un guide pour nouvelleaux\
> 251 - Ajouts et modifications diverses du contenu\
> 397 - proposition d'un premier style proche des TUI, les couleurs sont provisoires\
> 397 - mise à jour des critères pour le design du site\
> 394 - intégration du CSS de Derek\
> 394 - header > head\
> 394 - style.css comme dépendance de page\
> 394 - virer 1 niveau d'arbo\
> 394 - Ajout du lien de la vidéo pour une intervention aux techdays\
> 394 - Ajout du transcript d'une conversation avec chatgpt\
> 394 - Progrès sur la documentation de Francium\
> 394 - Progression sur la doc de Francium\
> 394 - Suppression du doc test laissé par Arthur\
> 394 - Ajout d'une nouvelle note sur la chrono du code du site\
