#! page

%T Katzele - identité et design
%A Derek Salmon
%P 2023-11-07
%S main

Cette note est un premier brouillon qui évoluera tout au long du processus de création de l'identité.
Aucune des notes, idées ci-dessous n'est définitive, tous les points peuvent être modifiés.

# Katzele

## Introduction

Nous sommes un collectif de personnes interrogeant la place et l'avenir du numérique dans une société contrainte de prendre en compte les limites planétaires. Les perspectives de décroissance à venir nous amènent à participer à la construction et à la diffusion d'un nouveau récit sur l'usage du numérique.

Les valeurs :

* Convivialisme
* Accessibilité
* @TODO Ajouter vos idées

## Principe du design et inspiration

J'utilise trois thèmes comme point de départ. Les interfaces TUI qui sont une inspiration naturelle pour le collectif. J'ajoute les visuels des chatons pour répondre à l'idée et de partager les outils, mais aussi pour donner un aspect plus ludique au TUI (ascii art, pixel art). Enfin, le mouvement solarpunk amène l'idée d'un imaginaire enviable et de la couleur. À noter que les couleurs sont aussi présentent dans les TUI, mais une couleur plus atténuée peut réduire l'aspect "terminal" qui fait encore "peur" au grand public. 

@TODO ajouter vos remarques, inspirations

## Principes techniques

Le design doit :

* Faciliter l'accessibilité du contenu et du site.
* Se baser sur des fonts système pour faciliter le chargement.
* Permettre d'utiliser des éléments du design dans un terminal (motif ascii, couleurs). @NOTE à expérimenter.
* Raspect l'aspect des TUI tout en évitant un aspect trop repoussant pour le grand public.
* Suivre les standards du web et éviter toute modification graphique qui s'éloigne de l'accessibilité et des habitudes du web (par exemple les liens souslignés).
* Éviter un aspect austère et donner envie tout en respectant des contraintes (limite de points, limite de la complexité du code, etc.).
* 100 Ko par page
* Moins de 200 lignes de CSS avec les commentaires
* Être facilement portable sur Gopher. @NOTE à discuter
* Servir de démonstration du convivialisme :
  * utiliser des outils convivialistes pour le développement
  * se limiter à des outils libres pour la partie graphique (Gimp, Dotgrid)
* Le processus doit être documenté.

@TODO Ajouter vos notes, modifier les éléments non pertinents, vos questions
