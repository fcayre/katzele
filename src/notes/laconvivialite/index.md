#! page

%T Notes sur \"La Convivialité\" d\'Ivan Illich
%A Arthur Pons
%P 2023-01-15
%S main

# Notes sur "La Convivialité" d'Ivan Illich"

Quelques notes sur le livre [La Convivialité] d'[Ivan Illich]. En vrac, des
passages qui me semblent pertinents pour les activités du collectif, des
passages que j'aimerais commenter, des passages que je ne comprends pas et dont
j'aimerais discuter. Si vous souhaitez avoir le texte complet contactez le
collectif.

Les mises en gras sont pas dans l'œuvre d'origine.

## Introduction

> Au stade avancé de la production de masse, une société produit sa propre
> destruction. La nature est dénaturée. L’homme déraciné, *castré dans sa
> créativité, est verrouillé dans sa capsule individuelle*. La collectivité est
> régie par le jeu combiné d’une polarisation exacerbée et d’une spécialisation
> à outrance. Le souci de toujours renouveler modèles et marchandises – usure
> rongeuse du tissu social – produit *une accélération du changement qui ruine
> le recours au précédent comme guide de l’action*.

Arthur - On parle souvent de créativité dans le collectif. L'idée qui semble
revenir est que l'outillage dont nous disposons principalement est générateur
de plus de créativité que certaines alternatives plus populaires.\ Je me
demande ce qu'il entend par "recours au précédent".

> On a du mal à imaginer une société où l’organisation industrielle serait
> équilibrée et compensée par des modes de production complémentaires, distincts
> et de haut rendement. Nous sommes tellement déformés par les habitudes
> industrielles que nous n’osons plus envisager le champ des possibles ; pour
> nous, renoncer à la production de masse, cela veut dire retourner aux chaînes
> du passé, ou reprendre l’utopie du bon sauvage.

Arthur - Cf. Macron et les amishs.\
Je ne sais pas si c'est approprié mais il est possible de trouver des exemples
dans l'industrie et les pratiques du logiciel.

> Il nous faut reconnaître que l’esclavage humain n’a pas été aboli par la
> machine, mais en a reçu figure nouvelle. Car, passé un certain seuil, l’outil,
> de serviteur, devient despote. Passé un certain seuil, la société devient une
> école, un hôpital, une prison. Alors commence le grand enfermement.

Arthur - Je pense saisir ce qu'il veut dire. Je me demande cependant comment
cette affirmation serait reçue par des personnes concernées directement par de
l'esclavage "ancienne figure".

> J’appelle société conviviale une société où l’outil moderne est au service de
> la personne intégrée à la collectivité, et non au service d’un corps de
> spécialistes. Conviviale est la société où l’homme contrôle l’outil. 

Arthur - Le passage souvent cité pour définir succinctement ce qu'est la
convivialité.

> Je suis conscient d’introduire un mot nouveau dans l’usage courant de la
> langue. Je fonde ma force sur le recours au précédent. Le père de ce vocable
> est Brillat-Savarin, dans sa Physiologie du goût : Méditations sur la
> gastronomie transcendantale. À moi de préciser, toutefois, que, dans
> l’acception quelque peu nouvelle que je confère au qualificatif, c’est l’outil
> qui est convivial et non l’homme. L’homme qui trouve sa joie et son équilibre
> dans l’emploi de l’outil convivial, je l’appelle austère. Il connaît ce que
> l’espagnol nomme la convivencialidad, il vit dans ce que l’allemand décrit
> comme la Mitmenschlichkeit. Car l’austérité n’a pas vertu d’isolation ou de
> clôture sur soi. Pour Aristote comme pour Thomas d’Aquin, elle est ce qui fonde
> l’amitié. En traitant du jeu ordonné et créateur, Thomas définit l’austérité
> comme une vertu qui n’exclut pas tous les plaisirs, mais seulement ceux qui
> dégradent la relation personnelle. L’austérité fait partie d’une vertu plus
> fragile qui la dépasse et qui l’englobe : c’est la joie, l’eutrapelia, l’amitié
> .

Arthur - Je vois toujours pas vraiment ce que veut dire recours au précédent.
C'est marrant, le qualificatif austère semble avoir des fondements pertinents
en philo mais est connoté tellement négativement aujourd'hui que je peine à
imaginer des écolos dirent volontairement d'eux mêmes qu'ils sont austère parce
qu'ils utilisent des outils conviviaux./
Mitmenschlichkeit est plus ou moins traduit "Compassion humaine", "amitié",
"humanité", "sollicitude".

D'ailleurs il existe une page d'homonymie wikipédia pour le mot [Convivialité]
dans laquelle il est stipulé que

> En informatique c'est la qualité d'un logiciel dont l'usage est intuitif ou
> qui dirige suffisamment son utilisateur pour ne nécessiter ni formation ni
> mode d'emploi (ce dernier sens est radicalement opposé au sens Illichien).

## 1 - Deux seuils de mutation

> L’année 1913 marque un tournant dans l’histoire de la médecine moderne. À peu
> près à partir de cette date, le patient a plus d’une chance sur deux qu’un
> médecin diplômé lui fournisse un traitement efficace – à condition, bien sûr,
> que son mal soit répertorié par la science médicale de l’époque. Familiers du
> milieu naturel, les chamans et les guérisseurs n’avaient pas attendu jusque-là
> pour prétendre à de pareils résultats dans un monde qui vivait dans un état de
> santé conçu différemment.

Arthur - [citation needed]

> Dans un certain sens, c’est l’industrialisation, plus que l’homme, qui a
> profité des progrès de la médecine. Les gens sont devenus capables de
> travailler plus régulièrement dans des conditions plus déshumanisantes. Pour
> cacher le caractère profondément destructeur du nouvel outillage, du travail à
> la chaîne et du règne de la voiture, on a monté en épingle des traitements
> spectaculaires appliqués aux victimes de l’agression industrielle sous toutes
> ses formes : vitesse, tension nerveuse, empoisonnement du milieu. *Et le médecin
> s’est transformé en mage, ayant seul le pouvoir de faire des miracles qui
> exorcisent la peur engendrée par la survie dans un monde devenu menaçant.*

Arthur - Est-ce que l'informaticienne et l'informaticien se sont transformé·es (ou sont
né·es) mages ?

> Au lendemain de la Seconde Guerre mondiale, il est devenu patent que la
> médecine moderne a de dangereux effets seconds. Mais il a fallu du temps aux
> médecins pour identifier la nouvelle menace représentée par les microbes rendus
> résistants à la chimiothérapie

Arthur - J'ai appris que la [chimiothérapie] ne concerne pas que la lutter
contre le cancer.

> Enfin, on a rendu impossible à la grand-mère, à la tante ou à la voisine de
> prendre en charge la femme enceinte, le blessé, le malade, l’infirme ou le
> mourant, ce qui a créé une demande impossible à satisfaire.

Arthur - Que des femmes. Forcément pas d'écriture inclusive, le bouquin a été
écrit en 73. Ceci n'est pas un début de procès.

> Fait significatif, le premier terrain de collaboration scientifique choisi par
> Nixon et Brejnev concerne les recherches sur les maladies des riches
> vieillissants.

Arthur - Marrant, [citation needed]

> Les dates de 1913 et 1955, que nous avons choisies comme indicatrices des deux
> seuils demutation, ne sont pas contraignantes. L’important est de comprendre
> ceci : au début du siècle la pratique médicale s’est engagée dans la
> vérification scientifique de ses résultats empiriques.  L’application de la
> mesure a marqué pour la médecine moderne le franchissement de son premier
> seuil. Le second seuil fut atteint lorsque l’utilité marginale du
> plus-de-spécialisation se mit à décroître, pour autant qu’elle soit
> quantifiable en termes de bien-être du plus grand nombre.

Arthur - Est-ce que cela ferait sens et si oui, est-il possible, de trouver des
équivalents à ces deux seuils pour le numériques ?

> La foi dans l’opération-miracle aveuglait le bon sens et ruinait l’antique
> sagesse en matière de santé et de guérison.

Arthur - Je pense que comme beaucoup de personnes je suis un peu gêné par le
discours d'Illich sur la médecine. Certaines choses me parlent, d'autre moins
et je suis hautement conscient que mon ignorance en la matière m'empêche de
savoir pourquoi. Parmi les choses qui me gênent, des expressions faisant, au
moins en surface, un peu "[appel à la nature]" ou "[appel à la tradition]". Je
devrais les lister.

> À présent, le coût social de la médecine n’est plus mesurable en termes
> classiques. Comment mesurer les faux espoirs, le poids du contrôle social, la
> prolongation de la souffrance, la solitude, la dégradation du patrimoine
> génétique et le sentiment de frustration engendrés par l’institution médicale ?

Arthur - Dans une certaine mesure je me pose la question avec le numérique.
Comment mesurer sa capacité de surveillance, de contrôle, la solitude qu'il
génère et les frustrations engendrées par des outils qui ne veulent pas se
laisser manipuler ?\
Sinon, "la dégradation du patrimoine génétique" ? Euuuh ?

> L’Américain type consacre, pour sa part, plus de 1500 heures par an à sa
> voiture : il y est assis, en marche ou à l’arrêt, il travaille pour la payer,
> pour acquitter l’essence, les pneus, les péages, l’assurance, les
> contraventions et les impôts. Il consacre donc quatre heures par jour à sa
> voiture, qu’il s’en serve, s’en occupe ou travaille pour elle. Et encore, ici
> ne sont pas prises en compte toutes ses activités orientées par le transport
> : le temps passéà l’hôpital, au tribunal ou au garage, le temps passé à
> regarder à la télévision la publicité automobile, le temps passé à gagner de
> l’argent pour voyager pendant les vacances, etc. À cet Américain, il faut
> donc 1500 heures pour faire 10 000 kilomètres de route ; 6 kilomètres lui
> prennent une heure.

Arthur - Je ne sais pas si ce calcul a été vérifié ou mis à jour depuis. Quoi
qu'il en soit c'est un bel exemple de dévoilement des coûts "totaux". Nous
avons souvent l'intuition que, de la même façon que l'on se trompe en pensant
que la voiture nous fait gagner du temps, on se trompe en pensant que
l'informatique nous fait *toujours* gagner du temps. Cela est malheureusement
très difficile à démontrer puisqu'une bonne partie des impacts, des changements
et donc des coûts induits par la numérisation d'un processus ou d'une
organisation sont très indirects. Ce sont des impacts quasi fantômes comme le
dit Guillaume Carninho. Je me demande donc s'il sera un jour possible, au moins
pour un sous-ensemble de tâches, de démontrer que l'on perd plus de temps à le
faire avec un ordinateur que sans. A noter qu'il y a deux échelles différentes
ici. Illich parle dans ce cas précis du temps pour l'individu, il serait
possible de se poser la question du temps pour un groupe d'individu ou
l'humanité toute entière.

## 2 - La reconstruction conviviale

### L'outil et la crise

> Les symptômes d'une crise planétaire qui va s'accélérant sont
> manifestes. On en a de tous côtés cherché le pourquoi. J'avance pour ma
> part l'explication suivante : la crise s'enracine dans l'échec de
> l'entreprise moderne, à savoir la substitution de la machine à l'homme.
> Le grand projet s'est métamorphosé en un implacable procès
> d'asservissement du producteur et d'intoxication du consommateur.

Arthur - Si par crise planétaire on entend crises environnementales, notamment
climatique, on pourrait dire qu'au contraire, c'est le "succès" du remplacement
de l'homme par la machine qui en est la cause.

> La dictature du prolétariat et la civilisation des loisirs sont deux variantes
> politiques de la même domination par un outillage industriel en constante
> expansion. L'échec de cette grande aventure fait conclure à la fausseté de
> l'hypothèse.

Arthur - Cette façon de renvoyer dos à dos un concept marxiste et le produit
de notre capitalisme me fait penser à ce que Bruno Latour et Nikolaj Schultz
ont écrit dans leur livre "Mémo sur la nouvelle classe écologique". Pour faire
très simple ils y écrivent que les socialistes et les libéraux avant eux
commettent tout deux l'erreur du productivisme. Que pour s'en détacher il faut,
de façon analogue aux socialistes, créer la conscience d'une nouvelle classe
(celle des écologistes) qui mobiliserait les personnes autour du projet d'une
société plus sobre et juste.

> La solution de la crise exige une radicale volte-face : ce n'est qu'en
> renversant la structure profonde qui règle le rapport de l'homme à
> l'outil que nous pourrons nous donner des outils justes. **L'outil juste
> répond à trois exigences : il est générateur d'efficience sans dégrader
> l'autonomie personnelle, il ne suscite ni esclaves ni maîtres, il
> élargit le rayon d'action personnel.** L'homme a besoin d'un outil *avec
> lequel travailler*, non d'un outillage qui *travaille à sa place*. Il a
> besoin d'une technologie qui tire le meilleur parti de l'énergie et de
> l'imagination personnelles, non d'une technologie qui l'asservisse et le
> programme.

Arthur - Ce passage est l'un des plus cités que j'ai pu trouver sur internet,
notamment la partie en gras. Elle est peut-être ce qui se rapproche le plus
d'une forme de définition structurée de ce qu'est un outil convivial.
D'ailleurs qu'il nomme ici "juste". Est-ce qu'il y a une différence ?
Cependant c'est la suite qui me parle davantage. Sans le détailler ici dans
l'écosystème UNIX et des outils types suckless/bitreich c'est bien *avec* les
outils que j'ai le sentiment de travailler. Je n'ai jamais eu autant d'idée -
et de capacité à les réaliser - que depuis que je pense avec ces outils à ma
disposition. Quand j'utilise des outils que je juge non convivial je ressens
fortement l'influence de la personne ou l'organisation qui la construit. Les
décisions qui ont été prises, le poids des fonctionnalités existantes ou le
manque de celles inexistantes. Quand j'utilise un outil philo UNIX je sens
l'influence de l'échafaudage sur lequel je me repose, construit à un coût
relativement modéré. Ça n'est pas une relation d'asservissement, celle qui se
matérialise par exemple dans le fait de découvrir que notre besoin très
spécifique est couvert par une fonctionnalité très profonde du logiciel.
C'est une relation qui au prix du formalisme de la manipulation du texte,
compromis raisonnable entre les natures de l'ordinateur et de l'humain,
offre des outils qui ne pensent rien de votre besoin et sauront spontanément
se rendre utiles quand votre imagination les sollicitera. Comme quand un
un lecteur de webradio, un gestionnaire de playlist et un agenda de concert
savent dialoguer sans qu'aucun n'ait été conçu pour ou ne sache même ce qu'un
artiste est.\
Je conçois qu'il puisse en être autrement pour les autres mais je n'ai pas de
raison de penser que nous soyons aussi peu dans le milieu de l'informatique.

## 5 - L'inversion politique

### La mutation soudaine

> Lorsque je parle de l’apparition de groupes d’intérêts et de leur
> préparation, je ne fais référence ni à des noyaux de terroristes ni à des
> dévots ni à des experts d’un nouveau genre. [...] la crise dont je décris la
> venue prochaine n’est pas intérieure à la société industrielle, elle concerne
> le mode industriel de production en lui-même. Cette crise oblige l’homme à
> choisir entre les outils conviviaux et l’écrasement par la méga-machine,
> entre la croissance indéfinie et l’acceptation des bornes
> multidimensionnelles. La seule réponse possible consiste à reconnaître sa
> profondeur et à accepter le seul principe de solution qui s’offre : établir,
> par accord politique, une autolimitation. Plus nombreux et divers en seront
> les hérauts, plus profonde sera la saisie de ce que le sacrifice est
> nécessaire, de ce qu’il protège des intérêts divers et qu’il est la base d’un
> nouveau pluralisme culturel.  *Je ne parle pas non plus d’une majorité opposée
> à la croissance au nom de principes abstraits. Ce serait une nouvelle
> majorité fantôme. À la vérité, la formation d’une élite organisée,
> chantant l’orthodoxie de l’anticroissance, est concevable. Cette élite est
> probablement en formation.* Mais un tel choeur, avec pour tout programme
> l’anticroissance, est l’antidote industriel à l’imagination révolutionnaire.
> En incitant la population à accepter une limitation de la production
> industrielle sans mettre en question la structure de base de la société
> industrielle, on donnerait obligatoirement plus de pouvoir aux bureaucrates
> qui optimisent la croissance, et on en deviendrait soi-même l’otage. La
> production stabilisée de biens et de services très rationalisés et
> standardisés éloignerait encore plus, si c’était possible, de la production
> conviviale que ne le fait la société industrielle de croissance.

Arthur - Je me sens un peu visé.

-----------------------------

## Outil

Avec `pandoc` et l'epub du bouquin (me contacter si vous le voulez) vous pouvez
faire

	pandoc la-convivialite.epub -o la-convivialite.txt

pour avoir un fichier texte format markdown du livre. Cela permet de facilement
prendre des notes dessus dans l'optique de produire un document similaire à
celui que vous êtes en train de lire. J'ai testé [epub2txt2] mais il ne semble
pas toujours bien gérer les accents.

[La Convivialité]: todo
[Ivan Illich]: https://fr.wikipedia.org/wiki/Ivan_Illich
[Convivialité]: https://fr.wikipedia.org/wiki/Convivialit%C3%A9
[Chimiothérapie]: https://fr.wikipedia.org/wiki/Chimioth%C3%A9rapie
[appel à la nature]: https://fr.wikipedia.org/wiki/Paralogisme_naturaliste
[appel à la tradition]: https://fr.wikipedia.org/wiki/Argumentum_ad_antiquitatem
[epub2txt2]: https://github.com/kevinboone/epub2txt2
%
