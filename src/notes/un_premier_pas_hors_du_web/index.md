#! page

%T Un premier pas hors du web
%A Derek Salmon, Arthur Pons
%P 2023-11-06
%S main

# Web convivialiste

Cette note a pour but d'introduire des outils numériques plus convivialistes pour le développement web basés sur une utilisation de commandes qui s'opposent à la tendance du web d'utiliser des outils de plus en plus complexes (builders, node.js, npm, etc.).

La création, la compréhension et l'utilisation d'outils déjà disponibles sur nos machines facilitent un premier apprentissage d'outils numériques convivialistes.

## Notes des besoins

- gestion des fichiers CSS
  - génération d'un ficher CSS import avec une liste de fichiers
  - merge de fichiers > envoyer les fichiers dans un dossier (variable)
  - minification
  - prefix (si pertinent)
  - détection d'erreurs
- gestion des fichiers HTML
  - minification
  - détection d'erreurs
    - code
      - [tidy](https://github.com/htacg/tidy-html5) ? 
    - accessibilité
- gestion des fichiers JS
  - merge de fichiers > envoyer les fichiers dans un dossier (variable)
  - minification
  - détection d'erreurs
- conversion / Compression
  - Fichier images, pdfs, videos
    - Images
      - Imagemagick
    - pdf
    - videos
      - ffmpeg
  - Action par dossier
  - Sprite
- aide au développement
  - Chercher des notes (@TODO, @SPOON, @NOTE) dans un dossier.
    - grep -rnw TODO ./*, si executé dans vim mettre le curseur au début d'une ligne et faire "gF" pour ouvrir le fichier à la bonne ligne
    - Générateur de structure d'un projet
    - Génerateur de TODO list, readme, git story
- gestion de la mise en ligne
  - déploiement 
  - sauvegarde
  - mise en ligne de changements par dossier
- gopher
  - Proposer des outils pour convertir un site pour Gopher (existant?)
    - Je crois pas malheureusement, ça ne devrait pas être troooop difficile

Ces outils doivent s'intégrer facilement sur une majorité de distribution. Idéalement sous forme de fonction qui accepte des arguments et qui peuvent facilement être appelés via des scripts et des alias. @NOTE: Est-ce que cette méthode est la plus pertinente.

Il est aussi intéressant de développer des sujets annexes pour limiter l'usage de plateformes. Je pense notamment à l'usage de GIT qui est trop souvent lié à Github et Gitlab. 

Enfin des outils comme VIM peuvent aussi remplacer des éditeurs de code qui sont souvent des applications lourdes.

## Une introduction vers un web plus simple

Sans rejeter l'aspect du web, c'est surtout les principes que nous lui associons qui le rendre nuisible. La vitesse, l'innovation constante, le fait d'être accessible et en ligne 24h sur 24h ne sont pas une réelle finalité du web, mais uniquement des solutions/contraintes techniques qui nous ont été imposées. Il est intéressant de regarder d'autres modèles d'internet qui existe et qui proposent d'autres imaginaires soit par contrainte ou par choix. Je pense notamment à trois modèles :

* Internet à Cuba (El Paquete
semanal, internet par la poste, Sneakernet)
* Dakar Internet project
* Réseau [Guifi](https://guifi.net/en) 

Un internet moins omniprésent semble possible et enviable à condition de comprendre les enjeux écologiques, sociétaux  et géopolitiques du numérique. Sans des prérequis un retour en arrière me paraît peu enviable pour la majorité des utilisateurs.

## Histoire du besoin

Nous sommes beaucoup à avoir une passion pour le web et la culture libre, mais notre temps est de plus en plus accaparé par un web qui se complexifie alors que les défis actuels demandent l'inverse. Nos outils, de plus en plus complexes limitent notre résilience et augmentent le risque de dépendance.

En réalité, le succès des outils web modernes dépend principalement du marketing des GAFAM (react avec Meta par exemple) et d'un effet de mode, mais surtout d'un manque de connaissances d'outils déjà présents sur nos machines qui peuvent réaliser des tâches similaires à moindre coût.
