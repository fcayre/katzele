# Comment utiliser Francium

Ce document existe pour apprendre à utiliser Francium sans pour autant lire
toute la documentation se trouvant aujourd'hui dans le README qui explique
également comment et pourquoi Francium fonctionne de la sorte.

A noter, Francium est conçu pour être modifiable et même inciter à l'être.
Ainsi ce document documente comment utiliser Francium tel qu'il est pour le
site de Katzele au début de l'année 2023. Il se peut donc que l'utilisation de
Francium soit très différente pour un autre projet ou à l'avenir. Des cas de
modifications relativement simples seront couverts à la fin de ce document mais
ils ne sauraient se substituer à la lecture du README.

## Dépendances

En l'état Francium nécessite

  * un logiciel md -> html (par défaut `cmark` mais pourrait être `pandoc` ou
    `lowdown` par exemple)
  * un interpréteur shell POSIX
  * perl si l'on utilise pug

## Générer le site

Francium génère le site via la commande `make`. Les règles de génération sont
écrites dans le fichier `makefile`. Pour lancer la génération faire

	make

Il est possible d'accélérer le processus en parallélisant 
Pour purger les fichiers produits faire

	make clean

Il n'est pas toujours nécessaire de faire un `clean` avant de faire un `make`,
les fichiers seront réécrits. La génération du site va transposer les fichiers
et l'arborescence source - dans `./src` - vers la destination - le dossier
`./root`. Ainsi avec l'arborescence

	./src
	├── archives
	│   └── index.md
	├── articles
	│   └── youtube
	│       ├── arguments-to-plumber.patch
	│       ├── display_author.patch
	│       └── index.md
	├── events
	│   └── 2022
	├── index.md
	└── notes
		└── laconvivialite
			└── index.md

`make` génére l'arborescence

	./root
	├── archives
	│   ├── index.html
	│   └── index.md
	├── articles
	│   └── youtube
	│       ├── arguments-to-plumber.patch
	│       ├── display_author.patch
	│       ├── index.html
	│       └── index.md
	├── events
	│   └── 2022
	├── events.atom
	├── index.html
	├── index.md
	├── notes
	│   └── laconvivialite
	│       ├── index.html
	│       └── index.md
	└── style.css

Pour chaque fichier présent dans `./src` le `makefile` a une règle expliquant à
`make` ce qu'il faut faire. Sans rentrer dans les détails, les fichiers `.md`
sont transformés en `.html` et copiés dans une arbo identique recrée sous
`./root`. Le fichier `2022` est traité avec le script `atomic` pour générer le
flux atom `events.atom`. Les autres fichiers, considérés par défaut comme
sortes d'annexes, et les sources md sont copiées telle quel dans l'arborescence
de façon à pouvoir y faire des liens dans les articles.

## Créer le contenu

L'exemple précédant se base sur le contenu du site Katzele mais vous voulez
évidemment faire le votre. Imaginons partir de zéro.

### Template HTML

Il nous faut au moins un template HTML qui structurera nos pages. Ce sera
l'échaffaudage pour nos pages. Ce template devra se trouver dans le dossier
`./lib/html`.

Il existe un outil nommé pug qui permet de générer des templates. Je ne sais
pas encore l'utiliser donc je vais pour le moment documenter le fait d'écrire
des templates à la main.

Prenons celui utilisé par le site Katzele qui a le mérite d'être basique

layout() <<@@ cat
	<!DOCTYPE html>
	<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>${title?La page dont le chemin s'affiche au dessus nécessite un titre}</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		${STYLE:+<link rel=\"stylesheet\" href=\"$STYLE\"/>}
		<meta name="description" content="$description">
	</head>
	<body>
		<main id="main" class="main">
			$(the main)
		</main>
	</body>
	</html>
@@

Les lignes `layout() <<@@ cat` et à la fin `@@` déclarent une fonction shell qui
contient un heredoc. Si vous voulez en savoir plus lisez le README. Vous pouvez
faire abstraction sinon.\
Le contenu qui se trouve entre ces deux lignes est un mélange d'HTML pur, de
variables shell et d'expansion de variables shell. Je ne vais pas les
expliciter ici. Ici ce template convient pour générer des pages très simples,
sans header ni footer, simplement le corps de la page que l'on écrit en
markdown. Si cela vous convient vous pouvez ne pas y toucher. Par défaut la
feuille de style qui sera utilisée est `./rc/style.css`. Elle contient le thème
de Katzele, vous pouvez la modifier à votre guise.

Pour d'éventuelles modifications du template ce qu'il faut à minima retenir est
que vous pouvez ici ajouter de l'HTML comme bon vous semble et avoir recours à
la commande `the nom_de_section` pour ajouter des sections que vous
renseignerez en markdown dans vos fichiers sources. Si vous voulez des exemples
de modification voir plus loin dans ce document.

### Fichier source markdown

Les fichiers sources des pages doivent comporter l'extension `.md` et être
exécutables. Ils doivent commencer par la ligne

	#! page

pour être préprocessé par le script `page`. Ils doivent ensuite contenir un
ensemble de métadonnée. En l'état Francium propose

  * `%T "titre de la page"` pour renseigner le contenu de la balise `<title>`,
    savoir ce qui s'affiche dans le titre de l'onglet de votre navigateur
  * `%A "nom de l'auteurice"` pour suivre qui a rédigé la page. Avec le
    template présenté auparavant cette donnée ne se retrouvera nul par dans
    l'HTML. Nous verrons un exemple de modification plus tard pour l'intégrer.
  * `%P "date de publication de la page"` pour suivre la date de publication.
    Elle peut ensuite être réutilisée dans la génération du site. Voir le
    README pour un exemple.
  * `%D "description du contenu"` pour alimenter la balise meta `description`,
    à savoir ce qui s'affichera comme contenu dans le résultat des moteurs de
    recherche par exemple.

On renseigne ces métadonnées en appelant, dans le fichier md, les différentes "variables".
Par exemple pour une page d'accueil

	%T "Page d'accueil de mon site"
	%A moi
	%P 2023-06-21
	%D "Site de machin chose, bidule à chouette à truc muche"

Il faut ensuite écrire le contenu markdown. Pour cela faire appel à
l'instruction `%S` marquant le début du markdown. Il faut mettre à la suite de
cette instruction la section du template dans laquelle on veut que le contenu
qui suive aille. Dans notre exemple de template il n'y a qu'une section
(marquée par la commande `$(the main)`) qui remplira bêtement la balise `main`,
unique balise du body. C'est le mot clef suivant `the` et non pas le nom de la
balise HTML qu'il faut retenir et faire correspondre avec ce que l'on va écrire
dans le fichier markdown. Il est naturel que la balise et le nom de la section
soient identiques mais cela n'est pas nécessaire. Pour y mettre du contenu nous
pouvons donc écrire :

	%S main
	# Le site de machin chose
	Salut !
	
	## Ma vie mon oeuvre
	
	J'ai travaillé ici, là-bas et accompli cela
	
	  * premier truc
	  * second truc
	%

Il faut fermer l'instruction `%S` avec un autre `%`. Au final notre document
ressemble à :

	#! page
	%T "Page d'accueil de mon site"
	%A moi
	%P 2023-06-21
	%D "Site de machin chose, bidule à chouette à truc muche"
	%S main
	# Le site de machin chose
	Salut !
	
	## Ma vie mon œuvre
	
	J'ai travaillé ici, là-bas et accompli cela
	
	  * premier truc
	  * second truc
	%

Si ce fichier se nomme `index.md` et se trouve à la racine du dossier `./src`
alors exécuter `make` génèrera ce `index.html` dans `./root` :

	<!DOCTYPE html>
	<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Page d'accueil de mon site</title>
		<meta name="viewport" content=
		"width=device-width,initial-scale=1.0">
		<link rel="stylesheet" href="/style.css">
		<meta name="description" content=
		"Site de machin chose, bidule à chouette à truc muche">
	</head>
	<body>
		<main id="main" class="main">
		<h1>Le site de machin chose</h1>
		<p>Salut !</p>
		<h2>Ma vie mon oeuvre</h2>
		<p>J'ai travaillé ici, là-bas et accompli cela</p>
		<ul>
			<li>premier truc</li>
			<li>second truc</li>
		</ul>
		</main>
	</body>
	</html>

Dernière astuce, en dehors des blocs `%S nom_de_section ... %` vous pouvez
exécuter du shell.  Ainsi il est possible, avec votre langage préféré, de
générer du markdown. Cela peut être pratique quand l'on veut générer quelque
chose en fonction d'un contenu qui change souvent et dont on ne connaît pas, à
priori, la teneur. Par exemple un plan de site. Pour ce faire écrivez le code
que vous souhaitez et pipez le dans la commande `save_md` suivant du nom de la
section dans laquelle vous voulez que le contenu apparaisse. Ainsi, en reprenant
la fin du document :

	J'ai travaillé ici, là-bas et accompli cela
	
	  * premier truc
	  * second truc
	%
	
	echo "[Un autre site cool](lien_vers_site_cool)" | save_md main

Ajoutera un lien dans la section main

		<li>premier truc</li>
		<li>second truc</li>
		</ul>
		<p><a href="lien_vers_site_cool">Un autre site cool</a></p>
		</main>
	</body>
	</html>

Cela peut être pratique pour générer tout ou partie d'un plan de site.
Pour un exemple plus utile lisez le README.

Voilà, à partir de là si vous savez écrire du markdown et que vous n'avez pas
besoin d'autre chose que des pages simplistes comme celles générées avec le
template fourni vous avez les cartes en main pour créer votre site avec
Francium. Cela dit l'esprit de l'outil est qu'il est de nature "hackable" avec
des compétences que l'on juge, dans le collectif, comme étant de bonnes
candidates pour être des compétences "socles" dans l'informatique (si c'est le
cas ou pas n'est pas le sujet de ce document).

Deux titres en dessous nous voyons quelques cas de modifications qui pourraient
vous intéresser ou vous mettre sur la bonne piste.

## Générer des flux atom

### Pour des évènements

Le script se trouvant à `./bin/atomic` permet de générer un flux atom sur la
base d'un fichier descriptif d'évènements. C'est un besoin assez particulier que
provient de l'activité du collectif Katzele. Si vous n'êtes pas intéressé·e par
cette fonctionnalité vous pouvez sauter ce titre.

Aujourd'hui au lancement de `make` Francium lit le fichier `./src/events/2022`
pour générer le flux `./root/events.atom`. Le nom du fichier (2022) est
arbitraire pour des raisons historiques mais devrait être modifié.\
TODO bah le faire...
Le fichier `2022` doit commencer avec `#! ./bin/atomic`. Il faut ensuite
déclarer le nom du flux et son url avec les métadonnées

  * %T - titre
  * %H - url du flux

Puis appeler l'instruction `entries` qui marque le début des éléments du flux.

La déclaration des évènements est assez similaire à l'écriture d'un article
(la technique derrière est la même). Les métadonnées disponibles (rappelées au
début du fichier) sont :

  * %T - title ou titre de l'évènement
  * %P - published the ou publié le
  * %L - location ou lieu de l'évènement
  * %A - author ou auteurice de l'évènement, qui du collectif ou associé y a
    participé ?
  * %D - date de l'évènement au format ISO 8601
  * %U - date de fin l'évènement au format ISO 8601
  * %R - ressources type slides, captation vidéo/audio, pdf etc
  * %S - contenu pour le flux rss, courte description de l'évènement

L'ordre d'apparition des champs est important, il doit être TPLADURS. Comme
pour une page, il faut fermer un bloc `%S` avec `%`.  Il est nécessaire d'échapper
les caractères spéciaux type `"` `|` `'` etc ou les encapsuler dans des cotes.
Les métadonnées de lieu, de ressources et le contenu peuvent être du markdown.\
Pour les évènements de Katzele tout cela donne l'exemple suivant :

	%T Les interventions du collectif Katzele
	%H http://people.netlib.re/events.atom
	
	entries
	
	%T Consommation alternative et sobre de youtube.com, le retour
	%P 2022-10-26T11:39:38+01:00
	%L Lab Numérique, Atrium, Campus de l\'Esplanade, Strasbourg
	%A Arthur Pons
	%D 2022-12-14T17:30+01:00
	%U 2022-12-14T19:30+01:00
	%R "[Article](http://people.netlib.re/notes/221019/interfaces_alternatives_youtube/index.html) - [Agenda](https://services-numeriques.unistra.fr/agenda/evenement/article/atelier-consommation-alternative-et-sobre-de-youtube-lab-numerique-1.html)"
	%S
	Vous en avez marre de rater les dernières sorties de vos youtubeurs et ...
	%

### Pour le contenu du site

Bientôt (Katzele time)
TODO intégrer cette fonctionnalité qui a été perdue lors de la spécialisation d'atomic pour les events
A priori y'aura rien à faire à part `make` ?

## Modifier Francium

Admettons que vous vouliez apporter les modifications suivantes à l'existant :

  * Gestion d'une métadonnée "langue" car le site est bilingue
  * Ajouter une section au template

### Ajouter une métadonnée

Vous l'avez peut-être remarqué, il existe une balise `<html lang="fr">` dans
notre template.  Cette balise sert à déclarer la langue du contenu qui suit.
Cela permet entre autre aux lecteurs d'écrans de lire correctement le contenu
de la page. Si vous tenez un site bilingue vous pourriez vouloir spécifier la
langue article par article et non pas de façon figée dans le template.

Pour cela ajoutons une métadonnée qui nous utiliserons au début des articles.
Pour ajouter une métadonnée il nous faut modifier le fichier `page` qui
contient le code pour les rendre opérantes. Dans ce fichier, ajouter une
métadonnée revient à déclarer un nouvel alias et une fonction correspondante.
En ce basant sur ce qui existe pour `%T` :

	alias %L="lang"
	lang() lang="$*"

Dorénavant quand on écrira `%L fr" dans un fichier, la variable `lang` prendra
pour valeur "fr". Il suffit ensuite de l'appeler au bon endroit dans le template

	layout() <<@@ cat
		<!DOCTYPE html>
		<html lang="$lang">
		<head>

Hop, nous pouvons dorénavant déclarer la langue d'un article. Nous pourrions
mettre un petit filet pour nous assurer de ne pas oublier d'un déclarer un :

	<html lang="${lang?Langue de l'article non déclarée}">

Si la variable est vide (parce que nous aurions oublié de renseigner `%L` dans
un article) alors la génération du site va se terminer et le message d'erreur
sera affiché.  Alternativement il est possible de choisir une valeur par
défaut. Pour plus d'info lire le README ou consulter le manuel de `sh`.

### Ajouter une section au template

Admettons que nous souhaitons ajouter un footer et un aside donc nous voulons
maitriser le contenu. Rien de plus simple, il suffit de créer les balises
qui vont bien et d'appeler la commande `the` avec les noms des sections.

	layout() <<@@ cat
	<!DOCTYPE html>
	<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>${title?La page dont le chemin s'affiche au dessus nécessite un titre}</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		${STYLE:+<link rel=\"stylesheet\" href=\"$STYLE\"/>}
		<meta name="description" content="$description">
	</head>
	<body>
		<main id="main" class="main">
			$(the main)
		</main>
		<aside id="aside" class="aside">
			$(the aside)
		</aside>
		<footer id="footer" class="footer">
			$(the footer)
			<p>$author</p>
		</footer>
	</body>
	</html>
	@@

Au passage nous mettons le nom de l'auteurice, se trouvant dans la variable
`$author` grâce à l'instruction `%A`, dans le footer.
Dans les sources d'une page il faudrait ensuite appeler `%S` avec les noms de
sections :

	%S main
	# Titre de l'article
	blablalba
	%
	
	%S aside
	truc que l'on raconte dans l'aside
	%
	
	%S footer
	contenu du footer
	%

A noter, si vous en avez une utilité quelconque, il est possible d'ouvrir et de
fermer les `%S` à votre guise. Le contenu d'une section sera généré dans l'ordre
d'apparition dans le document mais les sections n'interfèrent pas entre elles.\
Il est donc possible d'écrire :

	%S main
	# Titre
	azdazda
	%

	%S footer
	machin
	%

	%S main
	bidule
	%

	%S footer
	truc
	%

cela reviendrait à écrire


	%S main
	# Titre
	azdazda
	bidule
	%

	%S footer
	machin
	truc
	%

Pareil avec les appels à la commande `save_md nom_de_section` suite à un pipe.
